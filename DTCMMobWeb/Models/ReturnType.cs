﻿using DTCMMobWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTCMWebMobile.Models
{
    public class ReturnType
    {
        public string TicketValue  { get; set; }
        public string FullName { get; set; }

        public List<MetaData> ListAppWithPagesWithUserObjects = new List<MetaData>();
        public string ErrorMessage { get; set; }
        public bool IsAthen { get; set; }
        public bool AllowSnap { get; set; }



    }

    public class AppUser
    {
        public string Name { get; set; }
        public List<string> Pages = new List<string>();
       

    }
}