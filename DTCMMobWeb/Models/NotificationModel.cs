﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTCMMobWeb.Models
{
    public class NotificationModel
    {
        public string to { get; set; }
        public string priority { get; set; }
        public notification notification = new notification();
    }
    public class notification
    {
        public string body { get; set; }
        public string title { get; set; }
        public string sound { get; set; }
    }
}