﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTCMMobWeb.Models
{
    public class ImageModel
    {
        public string Name { get; set; }
        public string URL { get; set; }
        public int ID { get; set; }
    }
}