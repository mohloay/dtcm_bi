﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTCMMobWeb.Models
{
    public class UserPushTokenModel
    {
        public string UserPushToken { get; set; }
    }
    public class ReplaceUserPushTokenModel
    {
        public string OldUserPushToken { get; set; }
        public string NewUserPushToken { get; set; }
    }
}