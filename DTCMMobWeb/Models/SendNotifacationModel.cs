﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DTCMMobWeb.Models
{
    public class SendNotifacationModel
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Message { get; set; }

        public List<UserSelect> listUser { get; set; }
        public List<GroupSelect> listGroup { get; set; }
    }
    public class UserSelect
    {
        public string UserName { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Organization { get; set; }
        public bool IsSelected { get; set; }
    }
    public class GroupSelect
    {
        public string GroupName { get; set; }

        public bool IsSelected { get; set; }
}


}