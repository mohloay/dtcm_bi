﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTCMMobWeb.Models
{
    public class NameImg
    {
        public string name { get; set; }
        public List<string> images = new List<string>();
    }
}