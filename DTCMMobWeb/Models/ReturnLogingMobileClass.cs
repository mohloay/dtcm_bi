﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTCMMobWeb.Models
{
    public class ReturnLogingMobileClass
    {
        public bool CheckUser { get; set; }
        public string FullName { get; set; }
        public String ErorrMessage { get; set; }
        public bool AllowSnap { get; set; }
        public string Email { get; set; }
        public string Organization { get; set; }
        public List<string> listGroups = new List<string>();
    }
}