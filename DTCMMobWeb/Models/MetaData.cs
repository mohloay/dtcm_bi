using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DTCMMobWeb.Context;

namespace DTCMMobWeb.Models
{
    public class MetaData
    {
     public List<PageModel> pages = new List<PageModel>();
        public string name { get; set; }
        public Nullable<int> order { get; set; }
        public string namear { get; set; }
        public string nameen { get; set; }
        public string image { get; set; }
        public string description { get; set; }
        public string icon { get; set; }

    }

    public class PageModel 
    {
      public List<string> objects = new List<string>();
        public string name { get; set; }
        public string url { get; set; }
        public string namear { get; set; }
        public string nameen { get; set; }
        public string description { get; set; }
        public Nullable<int> DashboardID { get; set; }
        public string icon { get; set; }
        public Nullable<int> order { get; set; }
        public string FullNameEn { get; set; }
        public string FullNameAr { get; set; }
       public string RLID { get; set; }

  }
}
