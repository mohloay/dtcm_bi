function AdvertisingBusLogic() {
    BTCheckUserPrivilege("BT Dubai Advertising");
    $('.loading').on('DOMNodeInserted', function (e) {
        var object = e;
        var firstTime = true;
        if (firstTime) {
            setTimeout(function () {
                $($(object)[0].currentTarget).removeClass("loading");
            }, 3000);
            firstTime = false;
        }
    });
}

function BarriersBusLogic() {
    BTCheckUserPrivilege("BT Barriers");
    var FirstTime = true;
    $('#QV-Perception-Not_Considering_Dubai').on('DOMNodeInserted', function (e) {
        setTimeout(function () {
            if (FirstTime) {
                initSlider("slider1");
                FirstTime = false;
            }
        }, 1000);
    });
    $('.loading').on('DOMNodeInserted', function (e) {
        var object = e;
        var firstTime = true;
        if (firstTime) {
            setTimeout(function () {
                $($(object)[0].currentTarget).removeClass("loading");
            }, 3000);
            firstTime = false;
        }
    });
}

function ConsiderationBusLogic() {
    BTCheckUserPrivilege("BT Consideration Funnel ");
    $('.loading').on('DOMNodeInserted', function (e) {
        var object = e;
        var firstTime = true;
        if (firstTime) {
            setTimeout(function () {
                $($(object)[0].currentTarget).removeClass("loading");
            }, 3000);
            firstTime = false;
        }
    });
}


function PerceptionBusLogic() {
    BTCheckUserPrivilege("BT Perceptions by Pillars");
    var FirstTime = true;
    $('#QV-Perceptions-Shopping-Dubai').on('DOMNodeInserted', function (e) {
        setTimeout(function () {
            if (FirstTime) {
                initSlider("slider1");
                FirstTime = false;
                //BTLoadObj();
            }
        }, 1000);
    });
    $('.loading').on('DOMNodeInserted', function (e) {
        var object = e;
        var firstTime = true;
        if (firstTime) {
            setTimeout(function () {
                $($(object)[0].currentTarget).removeClass("loading");
            }, 3000);
            firstTime = false;
        }
    });
}


function WishlistBusLogic() {
    BTCheckUserPrivilege("BT Travel Wish List");
    $('.loading').on('DOMNodeInserted', function (e) {
        var object = e;
        var firstTime = true;
        if (firstTime) {
            setTimeout(function () {
                $($(object)[0].currentTarget).removeClass("loading");
            }, 3000);
            firstTime = false;
        }
    });
}


/// Helper Methods

function BTLoadObj() {
  LoadMashup(function () {
    $('.qvobject').each(function () {
      var appid = BrandTrakerDashAppValue;
      app = null;
      app = myqlik.openApp(BrandTrakerDashAppValue, config);
      var objectid = $(this).attr('objectid');
      var Selections = $(this).attr('Selection');
      var noInteraction = $(this).attr('noInteraction');
      if (SelectionAndInteractions == "true") {
        if (noInteraction == undefined) {
          if (Selections == undefined) {
            RenderObjectWithNonSelection(appid, objectid, $(this).attr('id'));
          } else
            RenderObject(appid, objectid, $(this).attr('id'))
        } else
          RenderObjectWithNonInteraction(appid, objectid, $(this).attr('id'));
      } else
        RenderObject(appid, objectid, $(this).attr('id'))
    });

  })
   
}


function BTCheckUserPrivilege(sheetName) {
    if (typeof BTPrivilegePagesPerUser == 'undefined') {
        LoadMashup(function () {
            app = myqlik.openApp(BrandTrakerDashAppValue, config);
            if (typeof BTPrivilegePagesPerUser == 'undefined') {
                app.getAppObjectList('sheet', function (reply) {
                    BTPrivilegePagesPerUser = new Array();
                    $.each(reply.qAppObjectList.qItems, function (key, value) {
                        BTPrivilegePagesPerUser.push(value.qData.title);
                    });
                    if (BTPrivilegePagesPerUser.indexOf(sheetName) > -1) {
                        BTLoadObj();
                    } else {
                        alert("not authorized To Access this page")

                    }

                }, config);

            } 

        });
    } else {
        if (BTPrivilegePagesPerUser.indexOf(sheetName) != -1) {
            //  ActiveTap(SelectedTap);
            BTLoadObj();
        } else {
            alert("not authorized To Access this page")

        }
    }
   
}

function BTNavBarFunctions() {
  $(".navbar-brand").unbind("click");
  $(".navbar-brand").on("click", function () {
    console.log("TPI Navbar");
    $(this).parent().next().toggle("slow");
    if (!app) {
      app = myqlik.openApp(BrandTrakerDashAppValue, config);
    }
    app.getObject("QV01", "QePWwJP");
    app.getObject("QV0222", "qspyHVw");
  });

}
