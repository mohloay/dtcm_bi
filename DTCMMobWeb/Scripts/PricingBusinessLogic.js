function AirlineBusLogic() {
    CheckUserPrivilege("Average price selected to Dubai");
    $('.loading').on('DOMNodeInserted', function (e) {
        var object = e;
        var firstTime = true;
        if (firstTime) {
            setTimeout(function () {
                $($(object)[0].currentTarget).removeClass("loading");
            }, 3000);
            firstTime = false;
        }
    });
}

function DetailsBusLogic() {
    CheckUserPrivilege("Airline Pricing and Booking behavior");
    var FirstTime = true;
    $('#QV-Ticket-Price-OverAll').on('DOMNodeInserted', function (e) {
        setTimeout(function () {
            if (FirstTime) {
                initSlider("slider1");
                FirstTime = false;
                //LoadObj();
            }
        }, 1000);
    });
    $('.loading').on('DOMNodeInserted', function (e) {
        var object = e;
        var firstTime = true;
        if (firstTime) {
            setTimeout(function () {
                $($(object)[0].currentTarget).removeClass("loading");
            }, 3000);
            firstTime = false;
        }
    });
}


/// Helper Methods

function LoadObj() {
  LoadMashup(function () {
    $('.qvobject').each(function () {
      app = null;
      var appid = PricingDashAppvalue;
      app = myqlik.openApp(PricingDashAppvalue, config);
      var objectid = $(this).attr('objectid');
      var Selections = $(this).attr('Selection');
      var noInteraction = $(this).attr('noInteraction');
      if (SelectionAndInteractions == "true") {
        if (noInteraction == undefined) {
          if (Selections == undefined) {
            RenderObjectWithNonSelection(appid, objectid, $(this).attr('id'));
          } else
            RenderObject(appid, objectid, $(this).attr('id'))
        } else
          RenderObjectWithNonInteraction(appid, objectid, $(this).attr('id'));
      } else
        RenderObject(appid, objectid, $(this).attr('id'))
    })
  })
   
}
function CheckUserPrivilege(sheetName) {
    if (typeof PrivilegePagesPerUser == 'undefined') {
        LoadMashup(function () {
            var app = myqlik.openApp(PricingDashAppvalue, config);
            if (typeof PrivilegePagesPerUser == 'undefined') {
                app.getAppObjectList('sheet', function (reply) {
                    PrivilegePagesPerUser = new Array();
                    $.each(reply.qAppObjectList.qItems, function (key, value) {
                        PrivilegePagesPerUser.push(value.qData.title);
                    });
                    if (PrivilegePagesPerUser.indexOf(sheetName) > -1) {
                        LoadObj();
                    } else {
                        alert("not authorized To Access this page")

                    }

                }, config);

            } 

        });

    } else {
        if (PrivilegePagesPerUser.indexOf(sheetName) != -1) {
            //  ActiveTap(SelectedTap);
            LoadObj();
        } else {
            alert("not authorized To Access this page");

        }
    }
}
