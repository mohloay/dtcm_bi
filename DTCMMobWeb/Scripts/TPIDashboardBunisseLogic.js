function SoruceMarketDashboard() {

    // alert("loaded");
    console.log("Soruce Market Dashboard");
    TPICheckUserPrivilege("Top 20 Source Markets", "sourcemarket");
    $("#sm_selector").addClass("hide_sm_selector");
    $("#divs_sm_selector").addClass("hide_sm_selector");
    $("#QV-timeperiod-selector").removeClass("hide_sm_selector");

    $('.loading').on('DOMNodeInserted', function (e) {
      var object = e;
      var firstTime = true;
      if (firstTime) {
        setTimeout(function () {
          $($(object)[0].currentTarget).removeClass("loading");
        }, 1000);
        firstTime = false;
      }
    });


}

function OverViewDashboard() {
  TPICheckUserPrivilege("Overview", "overview");
  $("#sm_selector").removeClass("hide_sm_selector");
  $("#divs_sm_selector").addClass("hide_sm_selector");
  $("#QV-month-selector").removeClass("hide_sm_selector");
  $("#QV-timeperiod-selector").removeClass("hide_sm_selector");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 1000);
      firstTime = false;
    }
  });
}

function DetailsDashBoard() {
  TPICheckUserPrivilege("Details", "details");
  $("#sm_selector").removeClass("hide_sm_selector");
  $("#divs_sm_selector").addClass("hide_sm_selector");
  $("#QV-month-selector").removeClass("hide_sm_selector");
  $("#QV-timeperiod-selector").removeClass("hide_sm_selector");
  LoadMashup(function () {
    var app = myqlik.openApp(TPIAPPValue, config);
    //Security on Object Level
    app.getAppObjectList('masterobject', function (reply) {
      var str = "";
      $.each(reply.qAppObjectList.qItems, function (key, value) {
        str += value.qMeta.title + ',';
        if (value.qMeta.title === "TPI MI Residence PCT KPI") {
          //document.getElementById("detail_residence_obj").setAttribute("class", "");
          //document.getElementById("detail_residence_caveat").setAttribute("class", "");
        } else if (value.qMeta.title === "TPI MI Hotel Hopping KPI") {
          //document.getElementById("detail_hh_obj").setAttribute("class", "");
          ////Collapse Net Hotel Guests KPI
          //document.getElementById("QV-p03-07").setAttribute("class", "h-sm-2 qvobject pct_residence");
        }

      });

      //alert(str);
    }, config);

  });
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 1000);
      firstTime = false;
    }
  });
}

function ExperienceExtrnlDashboard() {
  TPICheckUserPrivilege("TPI Experience External", "ExperienceExtrnl");

  $("#sm_selector").addClass("hide_sm_selector");
  $("#QV-month-selector").addClass("hide_sm_selector");
  $("#QV-timeperiod-selector").addClass("hide_sm_selector");
  $("#divs_sm_selector").removeClass("hide_sm_selector");
  var FirstTime = true;
  $('#QV-exp-ext-sat-unhappy-cy').on('DOMNodeInserted', function (e) {
    setTimeout(function () {
      if (FirstTime) {
        initSlider("slider1");
        initSlider("slider2");
        FirstTime = false;

        setTimeout(function () {
          $("#JTDgZdbList").change(function () {
            TPIfnSelectChange();
          });
          $("#JTDgZdbList").change();
          $(".clear").click(function () {
            setTimeout(function () {
              TPIfnSelectChange();
            }, 2000);
          });

        }, 1000);

      }
    }, 1000);
  });

  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 1000);
      firstTime = false;
    }
  });
}

function TPIfnSelectChange() {

  if ($("#JTDgZdbList").children("option:selected").text() == "Overall" || $("#JTDgZdbList").children("option:selected").text() == "Select Source Market") {
    $("#QV-exp-int-pillars-overall").show();
    $("#QV-exp-int-pillars-sm").hide();

  } else {
    $("#QV-exp-int-pillars-overall").hide();
    $("#QV-exp-int-pillars-sm").show();
  }
}

function fnYearSelectChange() {
  var sSelectedText = parseInt($(this).find('option:selected').text());
  if (sSelectedText <= 2015) {
    document.getElementById("QV-exp-int-pillars-nss-py-caveat").setAttribute("class", "qvobject");
  } else {
    document.getElementById("QV-exp-int-pillars-nss-py-caveat").setAttribute("class", "qvobject hide_object");
  }

}

function ExperienceIntrnlDashboard() {
  $("#sm_selector").addClass("hide_sm_selector");
  $("#QV-month-selector").addClass("hide_sm_selector");
  $("#QV-timeperiod-selector").addClass("hide_sm_selector");
  $("#divs_sm_selector").removeClass("hide_sm_selector");

  TPICheckUserPrivilege("TPI Experience Internal", "ExperienceIntrnl");
  // $scope.SelectedTapEx = SelectedTapEx1;
  var FirstTime = true;
  $('#QV-exp-ext-sat-unhappy-cy').on('DOMNodeInserted', function (e) {
    setTimeout(function () {
      if (FirstTime) {
        initSlider("slider1");
        initSlider("slider2");
        FirstTime = false;
        setTimeout(function () {
          $(window.top.document.body).on('change', '#divs_sm_selector .styled-select', TPIfnSelectChange);
          $('#divs_sm_selector .styled-select', window.top.document.body).trigger('change');
          $(".clear").click(function () {
            setTimeout(function () {
              TPIfnSelectChange();
            }, 2000);
          });

        }, 1000);
      }
    }, 1000);
  });
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 1000);
      firstTime = false;
    }
  });


}

function JourneyDashboard() {
  TPICheckUserPrivilege("Journey", "journey");
  // document.getElementById("sm_selector").setAttribute("class", "");
  $("#sm_selector").removeClass("hide_sm_selector")
  //document.getElementById("divs_sm_selector").setAttribute("class", "hide_sm_selector");
  $("#divs_sm_selector").addClass("hide_sm_selector");
  //document.getElementById("QV-month-selector").setAttribute("class", "form-group filter wow h-xs qvobject");
  $("#QV-month-selector").removeClass("hide_sm_selector");
  //document.getElementById("QV-timeperiod-selector").setAttribute("class", "form-group filter wow h-xs qvobject");
  $("#QV-timeperiod-selector").removeClass("hide_sm_selector");

  LoadMashup(function () {
    app = myqlik.openApp(TPIAPPValue, config);
    //Security on Object Level
    app.getAppObjectList('masterobject', function (reply) {
      var str = "";

      $.each(reply.qAppObjectList.qItems, function (key, value) {
        str += value.qMeta.title + ',';
        if (value.qMeta.title === "TPI MI Media Landscape") {
          $("#journey_media_landscape_obj").removeClass();
          $("#journey_media_landscape_caveat").removeClass();;
          //Resize Journey Container 
          $("#journey_container3").removeClass().addClass("col-lg-9");
        } else if (value.qMeta.title === "TPI MI Booking Method") {
          $("#journey_booking_method_obj").removeClass();
          $("#journey_booking_method_caveat").removeClass();
        } else if (value.qMeta.title === "TPI MI TS Key Travel Period" ||
          value.qMeta.title === "TPI MI TS School Term 1" ||
          value.qMeta.title === "TPI MI TS School Term 2" ||
          value.qMeta.title === "TPI MI TS School Term 3" ||
          value.qMeta.title === "TPI MI TS School Term 4" ||
          value.qMeta.title === "TPI MI TS Yearly Vacation Days"
        ) {
          $("#journey_travel_seasons_obj").removeClass();
          $("#journey_travel_seasons_caveat").removeClass();
        }


      });
    }, config);

  })
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 1000);
      firstTime = false;
    }
  });
}

function VolumeDashboard() {
  TPICheckUserPrivilege("volume", "volume");
  //document.getElementById("sm_selector").setAttribute("class", "");
  $("#sm_selector").removeClass("hide_sm_selector");
  //document.getElementById("divs_sm_selector").setAttribute("class", "hide_sm_selector");
  $("#divs_sm_selector").addClass("hide_sm_selector");
  //document.getElementById("QV-month-selector").setAttribute("class", "form-group filter wow h-xs qvobject");
  $("#QV-month-selector").removeClass("hide_sm_selector");
  //document.getElementById("QV-timeperiod-selector").setAttribute("class", "form-group filter wow h-xs qvobject");
  $("#QV-timeperiod-selector").removeClass("hide_sm_selector");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 1000);
      firstTime = false;
    }
  });
}

function TPICheckUserPrivilege(sheetName, SelectedTap) {
  if (typeof PrivilegePagesPerUser == 'undefined') {
    LoadMashup(function () {
      app = myqlik.openApp(TPIAPPValue, config);
      if (typeof PrivilegePagesPerUser == 'undefined') {
        app.getAppObjectList('sheet', function (reply) {
          PrivilegePagesPerUser = new Array();
          $.each(reply.qAppObjectList.qItems, function (key, value) {
            PrivilegePagesPerUser.push(value.qData.title);
          });
          if (PrivilegePagesPerUser.indexOf(sheetName) > -1) {

            TPILoadObj();
          } else {
            alert("not authorized To Access this page");
          }

        }, config);

      }

    });
  } else {
    if (PrivilegePagesPerUser.indexOf(sheetName) != -1) {
      TPILoadObj();
    } else {
      alert("not authorized To Access this page");

    }
  }


}

function TPILoadObj() {
  LoadMashup(function () {
    $('.qvobject').each(function () {
      app = null;
      app = myqlik.openApp(TPIAPPValue, config);
      var appid = TPIAPPValue;
      var objectid = $(this).attr('objectid');
      var Selections = $(this).attr('Selection');
      var noInteraction = $(this).attr('noInteraction');
      if (SelectionAndInteractions == "true") {
        if (noInteraction == undefined) {
          if (Selections == undefined) {
            RenderObjectWithNonSelection(appid, objectid, $(this).attr('id'));
          } else
            RenderObject(appid, objectid, $(this).attr('id'))
        } else
          RenderObjectWithNonInteraction(appid, objectid, $(this).attr('id'));
      } else
        RenderObject(appid, objectid, $(this).attr('id'))
    });

  })

}

function TPINavBarFunctions() {
  $(".navbar-brand").unbind("click");
  $(".navbar-brand").on("click", function () {
    console.log("TPI Navbar");
    $(this).parent().next().toggle("slow");
    if (!app) {
      app = myqlik.openApp(TPIAPPValue, config);
    }
    app.getObject("QV-year-selector", "f750b95b-de28-46e3-890a-e9c0aefa6f4b");
    app.getObject("QV-month-selector", "0ba069b0-0f04-428e-a2c3-d0b46984a7ed");
    app.getObject("QV-timeperiod-selector", "63035d00-8502-47a6-a956-cbb1ad6f4c30");
    app.getObject("sm_selector", "jzzRh");
    app.getObject("divs_sm_selector", "JTDgZ");
  });

}

// Main Busniess Logic
function MainBusLogic() {
  LoadMashup(function () {
    $('.qvobject').each(function () {
      app = myqlik.openApp(MainAPPValue, config);
      var appid = MainAPPValue;
      var objectid = $(this).attr('objectid');
      var Selections = $(this).attr('Selection');
      var noInteraction = $(this).attr('noInteraction');
      if (SelectionAndInteractions == "true") {
        if (noInteraction == undefined) {
          if (Selections == undefined) {
            RenderObjectWithNonSelection(appid, objectid, $(this).attr('id'));
          } else
            RenderObject(appid, objectid, $(this).attr('id'))
        } else
          RenderObjectWithNonInteraction(appid, objectid, $(this).attr('id'));
      } else
        RenderObject(appid, objectid, $(this).attr('id'))
    });

  })
}
