function OverviewDashBusLogic() {
  OSCheckUserPrivilege("Overview", "overview");
  var v_count = 0;
  LoadMashup(function () {
    var app = myqlik.openApp(OverSeasAPPValue, config);
    app.getAppObjectList('masterobject', function (reply) {
      var str = "";
      $.each(reply.qAppObjectList.qItems, function (key, value) {
        str += value.qMeta.title + ',';
        if (value.qMeta.title === "Overseas MI Seat Capacity to ME CY KPI" ||
          value.qMeta.title === "Overseas MI Seat Capacity to ME PY KPI" ||
          value.qMeta.title === "Overseas MI Seat Capacity to ME YoY KPI"
        ) {

          $("#CAPACITYTOMIDDLEEASTH").removeClass("hide_object");
          $("#CAPACITYTOMIDDLEEASTB").removeClass("hide_object");
          $("#overview_seat_capacity_to_me_caveat").removeClass();

          //Resize Overview Container
          // document.getElementById("overview_container").setAttribute("class", "col-lg-7 five-three");
          v_count = v_count + 1;
        } else if (value.qMeta.title === "Overseas MI Seat Capacity to World CY KPI" ||
          value.qMeta.title === "Overseas MI Seat Capacity to World PY KPI" ||
          value.qMeta.title === "Overseas MI Seat Capacity to World YoY KPI"
        ) {
          //$("#overview_seat_capacity_to_world_obj").removeClass();
          $("#OVERALLTOTALSEATCAPACITYH").removeClass("hide_object");
          $("#OVERALLTOTALSEATCAPACITYB").removeClass("hide_object");
          $("#overview_seat_capacity_to_world_caveat").removeClass();

          v_count = v_count + 1;
        }

      });

      if (v_count === 0) {
        document.getElementById("total_hotel_guests_title").textContent = "Total Hotel Guests";
      }
    }, config);

  });
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}

function LengthofstayDashBusLogic() {
  OSCheckUserPrivilege("Length of Stay", "lengthofstay");
  LoadMashup(function () {
    var app = myqlik.openApp(OverSeasAPPValue, config);
    //Security on Object Level
    app.getAppObjectList('masterobject', function (reply) {
      var str = "";
      $.each(reply.qAppObjectList.qItems, function (key, value) {
        str += value.qMeta.title + ',';
        if (value.qMeta.title === "Overseas MI PCT Residence by SM (YTD)") {

          $("#resident_pct_obj").removeClass("hide_object");
          $("#length_of_stay_obj").removeClass("hide_object");

        }

      });
    }, config);
  });
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}

function GuestsDashBusLogic() {
  OSCheckUserPrivilege("Hotel Guest Arrivals", "guests");
  var FirstTime = true;
  $('#QV-p04-01').on('DOMNodeInserted', function (e) {
    setTimeout(function () {
      if (FirstTime) {
        initSlider("slider1");
        initSlider("slider2");
        FirstTime = false;
      }
    }, 3000);
  });

  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}

function SeatcapacityDashboardBusLogic() {
  OSCheckUserPrivilege("Seat Capacity", "seatcapacity");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}

function SourceMarketDashBoardBusLogic() {
  OSCheckUserPrivilege("Top Source Market", "visitors");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}


function OVerSeasLoadObj() {
  LoadMashup(function () {
    $('.qvobject').each(function () {
      var appid = OverSeasAPPValue;
      app = null;
      app = myqlik.openApp(OverSeasAPPValue, config);
      var objectid = $(this).attr('objectid');
      var Selections = $(this).attr('Selection');
      var noInteraction = $(this).attr('noInteraction');
      if (SelectionAndInteractions == "true") {
        if (noInteraction == undefined) {
          if (Selections == undefined) {
            RenderObjectWithNonSelection(appid, objectid, $(this).attr('id'));
          } else
            RenderObject(appid, objectid, $(this).attr('id'))
        } else
          RenderObjectWithNonInteraction(appid, objectid, $(this).attr('id'));
      } else
        RenderObject(appid, objectid, $(this).attr('id'))
    })
  })

}

function OSCheckUserPrivilege(sheetName, SelectedTap) {
  if (typeof OVPrivilegePagesPerUser == 'undefined') {
    LoadMashup(function () {
      app = myqlik.openApp(OverSeasAPPValue, config);
      if (typeof PrivilegePagesPerUser == 'undefined') {
        app.getAppObjectList('sheet', function (reply) {
          OVPrivilegePagesPerUser = new Array();
          $.each(reply.qAppObjectList.qItems, function (key, value) {
            OVPrivilegePagesPerUser.push(value.qData.title);
          });
          if (OVPrivilegePagesPerUser.indexOf(sheetName) > -1) {

            OVerSeasLoadObj();
          } else {
            alert("not authorized To Access this page")

          }

        }, config);

      }

    });
  } else {
    if (OVPrivilegePagesPerUser.indexOf(sheetName) != -1) {
      //  ActiveTap(SelectedTap);
      OVerSeasLoadObj();
    } else {
      alert("not authorized To Access this page")

    }
  }


}

function OSNavbarFunctions() {
  $(".navbar-brand").unbind("click");
  $(".navbar-brand").on("click", function () {
    console.log("TPI Navbar");
    $(this).parent().next().toggle("slow");
    if (!app) {
      app = myqlik.openApp(TPIAPPValue, config);
    }
    app.getObject("QV01", "FLWSaH");
    app.getObject("QV0222", "EmtmKd");
    app.getObject("QV-TimePeriod-Filter", "YmYmRQ");
    app.getObject("QV03F", "DxSZvjx");
    app.getObject("QV04F", "bLbgLcE");

  });
}
