function DetailsBusLogic() {

  $(".navbar-fixed-top").show();
  HotelCheckUserPrivilege("HCAP Hotel Inventory by Location");
  var FirstTime = true;
  $('#QV-p04-021').on('DOMNodeInserted', function (e) {
    setTimeout(function () {
      if (FirstTime) {
        initSlider("slider1");
        FirstTime = false;
      }
    }, 1000);
  });
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function OutlookBusLogic() {
  $(".navbar-fixed-top").hide();

  HotelCheckUserPrivilege("HCAP Hotel Inventory");

  var FirstTime = true;
  $('#QV-HI-Hotel-Inventory-Trend').on('DOMNodeInserted', function (e) {
    setTimeout(function () {
      if (FirstTime) {
        initSlider("slider1");
        FirstTime = false;
        //HotLoadObj();
      }
    }, 1000);
  });
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}

function PipelineBusLogic() {
  $(".navbar-fixed-top").show();
  HotelCheckUserPrivilege("HCAP Hotel Room Capacity Increase by Area");
  var FirstTime = true;
  $('#QV-HCA-Hotel-Inventory-Change').on('DOMNodeInserted', function (e) {
    setTimeout(function () {
      if (FirstTime) {
        initSlider("slider1");
        FirstTime = false;
        //HotLoadObj();
      }
    }, 1000);
  });
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}


/// Helper Methods

function HotLoadObj() {
  app = null;
  LoadMashup(function () {
    $('.qvobject').each(function () {
      var appid = HotelPipeLineAPPValue;
      app = myqlik.openApp(HotelPipeLineAPPValue, config);
      var objectid = $(this).attr('objectid');
      var Selections = $(this).attr('Selection');
      var noInteraction = $(this).attr('noInteraction');
      if (SelectionAndInteractions == "true") {
        if (noInteraction == undefined) {
          if (Selections == undefined) {
            RenderObjectWithNonSelection(appid, objectid, $(this).attr('id'));
          } else
            RenderObject(appid, objectid, $(this).attr('id'))
        } else
          RenderObjectWithNonInteraction(appid, objectid, $(this).attr('id'));
      } else
        RenderObject(appid, objectid, $(this).attr('id'))
    })
  })

}


function HotelCheckUserPrivilege(sheetName) {
  if (typeof HotelPrivilegePagesPerUser == 'undefined') {
    LoadMashup(function () {
      app = myqlik.openApp(HotelPipeLineAPPValue, config);
      if (typeof HotelPrivilegePagesPerUser == 'undefined') {
        app.getAppObjectList('sheet', function (reply) {
          HotelPrivilegePagesPerUser = new Array();
          $.each(reply.qAppObjectList.qItems, function (key, value) {
            HotelPrivilegePagesPerUser.push(value.qData.title);
          });
          if (HotelPrivilegePagesPerUser.indexOf(sheetName) > -1) {
            HotLoadObj();
          } else {
            alert("not authorized To Access this page")

          }

        }, config);

      }

    });
  } else {
    if (HotelPrivilegePagesPerUser.indexOf(sheetName) != -1) {
      HotLoadObj();
    } else {
      alert("not authorized To Access this page")

    }
  }

}

function HotelNavBarFunctions() {
  $(".navbar-brand").unbind("click");
  $(".navbar-brand").on("click", function () {
    console.log("TPI Navbar");
    $(this).parent().next().toggle("slow");
    if (!app) {
      app = myqlik.openApp(HotelPipeLineAPPValue, config);
    }
    app.getObject("QV-HCA-Hotel-Status", "epkPPYG");
    app.getObject("QV-HCA-TimeFrame-Selector", "kdyppEr");
    app.getObject("QV-HIL-Hotel-Class-Selector", "MYLSmNp");
    app.getObject("QV-HIL-Hotel-Area-Selector", "mQFt");
    app.getObject("QV-HIL-Dev-Status-Selector", "fcTzYb");
  });

}



