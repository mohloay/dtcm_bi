function AdvocacyBusLogic() {
  VSCheckUserPrivilege("DIVS Advocacy");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}

function AdvocacyExtBusLogic() {
  VSCheckUserPrivilege("DIVS Advocacy External");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function AttractionsVisitedBusLogic() {
  VSCheckUserPrivilege("DIVS Attractions Visited");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function BookingChannelsBusLogic() {
  VSCheckUserPrivilege("DIVS Booking Channels");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}


function fnSelectChange() {
  if ($("#PEqDWdbList").children("option:selected").text() == "Overall") {
    $("#QV-pillars-overall").show();
    $("#QV-pillars-sm").hide();

  } else {
    $("#QV-pillars-overall").hide();
    $("#QV-pillars-sm").show();
  }
}

function InfrastructureBusLogic() {
  VSCheckUserPrivilege("DIVS Infrastructure");
  var FirstTime = true;
  $('#QV-p04-021').on('DOMNodeInserted', function (e) {
    setTimeout(function () {
      if (FirstTime) {
        initSlider("slider1");
        FirstTime = false;
        setTimeout(function () {
          $(window.top.document.body).on('change', '#QV_SM_Filter .styled-select', fnSelectChange);
          $('#QV_SM_Filter .styled-select', window.top.document.body).trigger('change');
          $(".clear").click(function () {
            setTimeout(function () {
              fnSelectChange();
            }, 2000);
          });

        }, 1000);
      }
    }, 1000);
  });
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function InfrastructureExtBusLogic() {
  VSCheckUserPrivilege("DIVS Infrastructure External");
  var FirstTime = true;
  $('#QV-p04-021').on('DOMNodeInserted', function (e) {
    setTimeout(function () {
      if (FirstTime) {
        initSlider("slider1");
        FirstTime = false;
        setTimeout(function () {
          $(window.top.document.body).on('change', '#QV_SM_Filter .styled-select', fnSelectChange);
          $('#QV_SM_Filter .styled-select', window.top.document.body).trigger('change');
          $(".clear").click(function () {
            setTimeout(function () {
              fnSelectChange();
            }, 2000);
          });

        }, 1000);
      }
    }, 1000);
  });
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });

}
function MediahabitsBusLogic() {
  VSCheckUserPrivilege("DIVS Media Habits");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function PillarsBusLogic() {
  VSCheckUserPrivilege("DIVS Pillars");
  $('#QV-p04-021').on('DOMNodeInserted', function (e) {
    var FirstTime = true;
    setTimeout(function () {
      if (FirstTime) {
        initSlider("slider1");
        FirstTime = false;
        setTimeout(function () {
          $(window.top.document.body).on('change', '#QV_SM_Filter .styled-select', fnSelectChange);
          $('#QV_SM_Filter .styled-select', window.top.document.body).trigger('change');
          $(".clear").click(function () {
            setTimeout(function () {
              fnSelectChange();
            }, 2000);
          });

        }, 1000);
      }
    }, 1000);
  });
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function PillarsExtBusLogic() {
  VSCheckUserPrivilege("DIVS Pillars External");
  $('#QV-p04-021').on('DOMNodeInserted', function (e) {
    var FirstTime = true;
    setTimeout(function () {
      if (FirstTime) {
        initSlider("slider1");
        FirstTime = false;
        setTimeout(function () {
          $(window.top.document.body).on('change', '#QV_SM_Filter .styled-select', fnSelectChange);
          $('#QV_SM_Filter .styled-select', window.top.document.body).trigger('change');
          $(".clear").click(function () {
            setTimeout(function () {
              fnSelectChange();
            }, 2000);
          });

        }, 1000);
      }
    }, 1000);
  });
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function RevisitBusLogic() {
  VSCheckUserPrivilege("DIVS Revisit");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function SatisfactionBusLogic() {
  VSCheckUserPrivilege("DIVS Satisfaction");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function SatisfactionExtBusLogic() {
  VSCheckUserPrivilege("DIVS Satisfaction External");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function SegmentsBusLogic() {
  VSCheckUserPrivilege("DIVS Segments of Purpose by Visit");
  var FirstTime = true;
  $('#QV-General').on('DOMNodeInserted', function (e) {
    setTimeout(function () {
      if (FirstTime) {
        initSlider("slider1");
        FirstTime = false;
      }
    }, 1000);
  });
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function SpendBusLogic() {
  VSCheckUserPrivilege("DIVS Spend");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function VisitorsdeMographicsBusLogic() {
  VSCheckUserPrivilege("DIVS Visitors Demographics");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
function VisitationBehaviourBusLogic() {
  VSCheckUserPrivilege("DIVS Visitation Behaviour");
  $('.loading').on('DOMNodeInserted', function (e) {
    var object = e;
    var firstTime = true;
    if (firstTime) {
      setTimeout(function () {
        $($(object)[0].currentTarget).removeClass("loading");
      }, 3000);
      firstTime = false;
    }
  });
}
/// Helper Methods


function VSLoadObj() {
  LoadMashup(function () {
    $('.qvobject').each(function () {
      var appid = DIVSAPPValue;
      app = myqlik.openApp(DIVSAPPValue, config);
      var objectid = $(this).attr('objectid');
      var Selections = $(this).attr('Selection');
      var noInteraction = $(this).attr('noInteraction');
      if (SelectionAndInteractions == "true") {
        if (noInteraction == undefined) {
          if (Selections == undefined) {
            RenderObjectWithNonSelection(appid, objectid, $(this).attr('id'));
          } else
            RenderObject(appid, objectid, $(this).attr('id'))
        } else
          RenderObjectWithNonInteraction(appid, objectid, $(this).attr('id'));
      } else
        RenderObject(appid, objectid, $(this).attr('id'))
    });
  })

}


function VSCheckUserPrivilege(sheetName) {
  if (typeof VSPrivilegePagesPerUser == 'undefined') {
    app = null;
    LoadMashup(function () {
      app = myqlik.openApp(DIVSAPPValue, config);
      if (typeof VSPrivilegePagesPerUser == 'undefined') {
        app.getAppObjectList('sheet', function (reply) {
          VSPrivilegePagesPerUser = new Array();
          $.each(reply.qAppObjectList.qItems, function (key, value) {
            VSPrivilegePagesPerUser.push(value.qData.title);
          });
          if (VSPrivilegePagesPerUser.indexOf(sheetName) > -1) {
            VSLoadObj();
          } else {
            alert("not authorized To Access this page")

          }

        }, config);

      }
    });
  } else {
    if (VSPrivilegePagesPerUser.indexOf(sheetName) != -1) {
      //  ActiveTap(SelectedTap);
      VSLoadObj();
    } else {
      alert("not authorized To Access this page")

    }
  }

}


function VSNavBarFunctions() {
  $(".navbar-brand").unbind("click");
  $(".navbar-brand").on("click", function () {
    console.log("TPI Navbar");
    $(this).parent().next().toggle("slow");
    if (!app) {
      app = myqlik.openApp(DIVSAPPValue, config);
    }
    app.getObject("QV_Time_Period_Filter", "vBYJbn");
    app.getObject("QV_SM_Filter", "PEqDW");
  });

}
