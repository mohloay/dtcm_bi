﻿using QlikAuthNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace DTCMMobWeb.Generic_Function
{
    public class TestClass : Ticket
    {
        TestClass()
        {

            this.CertificateLocation = StoreLocation.LocalMachine;
            this.CertificateName = "QlikClient";
        }
    }
}