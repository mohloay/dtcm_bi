using DTCMMobWeb.Context;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace DTCMMobWeb.Generic_Function
{
    public class AuthAttr : ActionFilterAttribute 
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            HttpRequestMessage reqest = new HttpRequestMessage();
            var headers = actionContext.Request.Headers;
            if (headers.Contains("Token"))
            {
                string tokenVale = headers.GetValues("Token").First();
           dashboard_metadataEntities dTCMDBEntities = new dashboard_metadataEntities();
                var userToken = dTCMDBEntities.Users.FirstOrDefault(token => token.UserToken == tokenVale);
                if (userToken == null || (DateTime.Now > userToken.CreationDate.Value.AddMinutes(30) ))
                {
                    HttpResponseMessage httpResponseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                    httpResponseMessage.ReasonPhrase = "error in the Token OR it's Not contian in the header";
                    actionContext.Response = httpResponseMessage;
                }
                else
                {
                    userToken.CreationDate = DateTime.Now.AddMinutes(30);
                    dTCMDBEntities.SaveChanges();
                }
            }
            else
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                actionContext.Response = httpResponseMessage;
            }

            
           
           
        }
    }
}
