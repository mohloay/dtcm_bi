﻿using DTCMWebMobile.Models;
using QlikAuthNet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.DirectoryServices;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.DirectoryServices.AccountManagement;
using DTCMMobWeb.Models;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using DTCMMobWeb.Generic_Function;

namespace DTCMWebMobile.Generic_Function
{
    public class UserFunction
    {
        public static string hash = "4h&bn9873*";
        private const string _alg = "HmacSHA256";
        private const string _salt = "rz8LuOtFBXphj9WQfvFh"; // Generated at https://www.random.org/strings
        public static string GenerateToken(string username, string password)
        {
            string hash = string.Join(":", new string[] { username , Convert.ToString(DateTime.Now.Hour) });
            string hashLeft = "";
            string hashRight = "";
            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                hmac.Key = Encoding.UTF8.GetBytes(GetHashedPassword(password));
                hmac.ComputeHash(Encoding.UTF8.GetBytes(hash));
                hashLeft = Convert.ToBase64String(hmac.Hash);
                hashRight = string.Join(":", new string[] { username , Convert.ToString(DateTime.Now.Hour) });
            }
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", hashLeft, hashRight)));
        }
        public static string GetHashedPassword(string password)
        {
            string key = string.Join(":", new string[] { password, _salt });
            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                // Hash the key.
                hmac.Key = Encoding.UTF8.GetBytes(_salt);
                hmac.ComputeHash(Encoding.UTF8.GetBytes(key));
                return Convert.ToBase64String(hmac.Hash);
            }
        }
        public static bool CheckUset(UserLogin userLogin , ref string USerGroup)
        {
            string ServerName = WebConfigurationManager.AppSettings["ADServerName"];
            string AtValue = WebConfigurationManager.AppSettings["ADDmainNameWtihAt"];
            string AddDomainValue = WebConfigurationManager.AppSettings["ADDomainName"];
            string UserName;
            if (AtValue == "1")
            {
                UserName = userLogin.UserName + "@" + AddDomainValue;
            }
            else
            {
                UserName = AddDomainValue + "\\" + userLogin.UserName;
            }
            DirectoryEntry entry = new DirectoryEntry("LDAP://" + ServerName,
           UserName, userLogin.Password);
            try
            {
                // Bind to the native AdsObject to force authentication.
                Object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);
                string UserID = userLogin.UserName;
                search.Filter = "(SAMAccountName=" + UserID + ")";
                search.PropertiesToLoad.Add("memberOf");
                StringBuilder groupNames = new StringBuilder(); //stuff them in | delimited

                SearchResult result = search.FindOne();
                int propertyCount = result.Properties["memberOf"].Count;
                String dn;
                int equalsIndex, commaIndex;

                for (int propertyCounter = 0; propertyCounter < propertyCount; propertyCounter++)
                {
                    dn = (String)result.Properties["memberOf"][propertyCounter];

                    equalsIndex = dn.IndexOf("=", 1);
                    commaIndex = dn.IndexOf(",", 1);
                    
                    groupNames.Append(dn.Substring((equalsIndex + 1),
                                (commaIndex - equalsIndex) - 1));
                    groupNames.Append("|");
                }
                USerGroup = groupNames.ToString();

            }
            catch(Exception ex)
            {

                return false;
            }
            return true;

        }
        public static ReturnLogingMobileClass CheckMobileUSer(UserLogin userLogin)
        {
            ReturnLogingMobileClass returnLogingMobileClass = new ReturnLogingMobileClass();
            returnLogingMobileClass.CheckUser = false;
            returnLogingMobileClass.AllowSnap = false;
            string ServerName = WebConfigurationManager.AppSettings["ADServerName"];
            string AtValue = WebConfigurationManager.AppSettings["ADDmainNameWtihAt"];
            string AddDomainValue = WebConfigurationManager.AppSettings["ADDomainName"];
            var AccessMobileGroups = WebConfigurationManager.AppSettings["MobileAccessGroup"].Split(',');
            var MobileAccessSnap = WebConfigurationManager.AppSettings["MobileAccessSnap"].Split(',');
            string UserName;
            if (AtValue == "1")
            {
                UserName = userLogin.UserName + "@" + AddDomainValue;
            }
            else
            {
                UserName = AddDomainValue + "\\" + userLogin.UserName;
            }
            DirectoryEntry entry = new DirectoryEntry("LDAP://" + ServerName,
           UserName, userLogin.Password);
            try
            {
                // Bind to the native AdsObject to force authentication.
                Object obj = entry.NativeObject;
                DirectorySearcher search = new DirectorySearcher(entry);
                string UserID = userLogin.UserName;
                search.Filter = "(SAMAccountName=" + UserID + ")";
                search.PropertiesToLoad.Add("displayname");
                search.PropertiesToLoad.Add("CN");
                search.PropertiesToLoad.Add("ou");
                search.PropertiesToLoad.Add("mail");
                search.PropertiesToLoad.Add("memberOf");
                StringBuilder groupNames = new StringBuilder(); //stuff them in | delimited
                SearchResult result = search.FindOne();
                int propertyCount = result.Properties["memberOf"].Count;
                String dn;
                int equalsIndex, commaIndex;
                returnLogingMobileClass.FullName =  result.Properties["displayname"][0].ToString();
                if(result.Properties["mail"].Count > 0)
                    returnLogingMobileClass.Email = result.Properties["mail"][0].ToString();
                returnLogingMobileClass.Organization = String.Join(",", result.Path.Split(',').Where(s => s.Contains("OU")).ToList() ).Replace("OU=", "");
                for (int propertyCounter = 0; propertyCounter < propertyCount; propertyCounter++)
                {
                    dn = (String)result.Properties["memberOf"][propertyCounter];

                    equalsIndex = dn.IndexOf("=", 1);
                    commaIndex = dn.IndexOf(",", 1);

                    groupNames.Append(dn.Substring((equalsIndex + 1),
                                (commaIndex - equalsIndex) - 1));
                    groupNames.Append("|");
                }
                var groupsUser = groupNames.ToString().Split('|');
                returnLogingMobileClass.listGroups.AddRange(groupsUser);
                foreach (var item in groupsUser)
                {
                    if (AccessMobileGroups.Contains(item))
                    {
                        returnLogingMobileClass.CheckUser = true;
                    }
                }

                if (!returnLogingMobileClass.CheckUser)
                {
                    returnLogingMobileClass.ErorrMessage = "Your group not authorized to use mobile application ";
                }
                else
                {
                    foreach (var item in MobileAccessSnap)
                    {
                        if (groupsUser.Contains(item))
                        {
                            returnLogingMobileClass.AllowSnap = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                returnLogingMobileClass.CheckUser = false;
                if (!ex.Message.Contains("The server is not operational"))
                    returnLogingMobileClass.ErorrMessage = "Username or Password is incorrect";
                else
                    returnLogingMobileClass.ErorrMessage = "Unable to authenticate the user";
                UserFunction.LogError("Active Directory", ex);
                return returnLogingMobileClass;
            }
            returnLogingMobileClass.listGroups.Remove("");
            return returnLogingMobileClass;

        }

        public static String GetTicket(UserLogin userLogin)
        {
            string UserID = userLogin.UserName;
            var req = new Ticket
            {
                UserDirectory = WebConfigurationManager.AppSettings["DomainQlikDir"],// "EXCEEDLABS",
                UserId = UserID,
            };

            req.CertificateLocation = System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine;
           
            //Not need when the request is initated from the Virtual Proxy  

            string host = WebConfigurationManager.AppSettings["QlikServerIP"];

            string uri = host + ":4243/qps/custom";
            req.ProxyRestUri = uri;
            return req.TicketRequest();
        }

        public static String Encrypt(string TXT)
        {
            
           Random random = new Random();
           const string chars = "%4h&bn9873*7^><?:'";
           var ss = new string(Enumerable.Repeat(chars, 7).Select(s => s[random.Next(s.Length)]).ToArray());

            byte[] data = UTF8Encoding.UTF8.GetBytes(TXT);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(ss));
                using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                {
                    ICryptoTransform transform = tripDes.CreateEncryptor();
                    byte[] result = transform.TransformFinalBlock(data, 0, data.Length);
                    return Convert.ToBase64String(result, 0, result.Length)+"THO"+ss ;
                }
            }
        }

        public static String Decryot(string TXT)
        {
            var CurrentST = TXT.Split(new string[] { "THO" }, StringSplitOptions.None);
            byte[] data = Convert.FromBase64String(CurrentST[0]);
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(CurrentST[1]));
                using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                {
                    ICryptoTransform transform = tripDes.CreateDecryptor();
                    byte[] result = transform.TransformFinalBlock(data, 0, data.Length);
                    return UTF8Encoding.UTF8.GetString(result);
                }
            }
        }

        public static void SendNofticationMessage(string Title , string MessageBody , string PNToken)
        {
            try
            {
                WebRequest tRequest = WebRequest.Create(WebConfigurationManager.AppSettings["FirebdURL"]);
                tRequest.Method = "post";
                //serverKey - Key from Firebase cloud messaging server  
                tRequest.Headers.Add(string.Format("Authorization:" + WebConfigurationManager.AppSettings["AuthorizationKey"]));
                //Sender Id - From firebase project setting  
                tRequest.Headers.Add(string.Format("Sender: " + WebConfigurationManager.AppSettings["SenderId"]));
                tRequest.ContentType = "application/json";
                NotificationModel notificationModel = new NotificationModel
                {
                    to = PNToken
                    ,
                    priority = "high",
                    notification = new notification() { body = MessageBody, title = Title, sound = "sound.caf" }
                };
                var serializer = new JavaScriptSerializer();
                Byte[] byteArray = Encoding.UTF8.GetBytes(serializer.Serialize(notificationModel));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String sResponseFromServer = tReader.ReadToEnd();
                                    var sssss = sResponseFromServer;
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UserFunction.LogError("Send Notofaication Error", ex);
            }
        }

        public static void LogError(string ErrorType , Exception ex )
        {
            try
            {
                StreamWriter sw = new StreamWriter(@"" + (WebConfigurationManager.AppSettings["LogErrorFilePath"].ToString()), true);
                sw.WriteLine("ErrorLocation:" + ErrorType + " Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                       "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                sw.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                sw.Close();
            }
            catch {
                string filePath = HttpContext.Current.Server.MapPath("/LogError.txt");
                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    writer.WriteLine("ErrorLocation:" + ErrorType + " Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                       "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                    writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                }
            }
        }
    }
}
