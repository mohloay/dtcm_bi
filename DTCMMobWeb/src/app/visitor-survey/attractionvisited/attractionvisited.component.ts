import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy} from '@angular/core';
declare var AttractionsVisitedBusLogic: any;
@Component({
  selector: 'Attractions',
  templateUrl: './attractionvisited.component.html',
  styleUrls: ['./attractionvisited.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class AttractionvisitedComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      AttractionsVisitedBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      AttractionsVisitedBusLogic();
    }


  }

}
