import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttractionvisitedComponent } from './attractionvisited.component';

describe('AttractionvisitedComponent', () => {
  let component: AttractionvisitedComponent;
  let fixture: ComponentFixture<AttractionvisitedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttractionvisitedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttractionvisitedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
