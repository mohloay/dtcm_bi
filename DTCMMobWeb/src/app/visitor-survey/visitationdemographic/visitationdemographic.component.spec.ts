import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitationdemographicComponent } from './visitationdemographic.component';

describe('VisitationdemographicComponent', () => {
  let component: VisitationdemographicComponent;
  let fixture: ComponentFixture<VisitationdemographicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitationdemographicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitationdemographicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
