import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var VisitorsdeMographicsBusLogic: any;
@Component({
  selector: 'Demographics',
  templateUrl: './visitationdemographic.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class VisitationdemographicComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      VisitorsdeMographicsBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      VisitorsdeMographicsBusLogic();
    }
  }
}
