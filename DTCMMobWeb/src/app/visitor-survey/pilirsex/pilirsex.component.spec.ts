import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PilirsexComponent } from './pilirsex.component';

describe('PilirsexComponent', () => {
  let component: PilirsexComponent;
  let fixture: ComponentFixture<PilirsexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilirsexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PilirsexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
