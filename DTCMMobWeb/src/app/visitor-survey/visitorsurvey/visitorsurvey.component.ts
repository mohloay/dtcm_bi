import { Component, OnInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var VSNavBarFunctions: any;
@Component({
  selector: 'app-visitorsurvey',
  templateUrl: './visitorsurvey.component.html',
  styleUrls: ['./visitorsurvey.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class VisitorsurveyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    VSNavBarFunctions();
  }

}
