import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorsurveyComponent } from './visitorsurvey.component';

describe('VisitorsurveyComponent', () => {
  let component: VisitorsurveyComponent;
  let fixture: ComponentFixture<VisitorsurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitorsurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitorsurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
