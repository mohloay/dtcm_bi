import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routedComponents, VisitorsurveyRoutingModule } from './visitorsurvey-routing.module';

@NgModule({
  declarations: [routedComponents],
  imports: [
    CommonModule,
    VisitorsurveyRoutingModule
  ]
})

export class VisitorSurveyModule { }
