import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var RevisitBusLogic: any;
@Component({
  selector: 'Revisit',
  templateUrl: './revisit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class RevisitComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      RevisitBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      RevisitBusLogic();
    }
  }
}
