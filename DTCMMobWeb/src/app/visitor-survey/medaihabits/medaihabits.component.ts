import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var MediahabitsBusLogic: any;
@Component({
  selector: 'MediaHabits',
  templateUrl: './medaihabits.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush

})
@Injectable()
export class MedaihabitsComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      MediahabitsBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      MediahabitsBusLogic();
    }
  }
}
