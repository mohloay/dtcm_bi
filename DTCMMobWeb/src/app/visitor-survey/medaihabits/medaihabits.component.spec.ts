import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedaihabitsComponent } from './medaihabits.component';

describe('MedaihabitsComponent', () => {
  let component: MedaihabitsComponent;
  let fixture: ComponentFixture<MedaihabitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedaihabitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedaihabitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
