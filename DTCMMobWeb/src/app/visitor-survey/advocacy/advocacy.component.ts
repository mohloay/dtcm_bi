import { Component ,AfterViewInit, Injectable, ChangeDetectionStrategy} from '@angular/core';
declare var AdvocacyBusLogic: any;
@Component({
  selector: 'Advocacy',
  templateUrl: './advocacy.component.html',
  styleUrls: ['./advocacy.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class AdvocacyComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      AdvocacyBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      AdvocacyBusLogic();
    }


  }

}
