import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var InfrastructureBusLogic: any;
@Component({
  selector: 'Infrastructure',
  templateUrl: './infrastructure.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class InfrastructureComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      InfrastructureBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      InfrastructureBusLogic();
    }
  }
}
