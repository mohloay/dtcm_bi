import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var SatisfactionBusLogic: any;
@Component({
  selector: 'Satisfaction',
  templateUrl: './satisfaction.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class SatisfactionComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      SatisfactionBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      SatisfactionBusLogic();
    }
  }
}
