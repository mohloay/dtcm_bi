import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdvocacyComponent } from './advocacy/advocacy.component';
import { AdvocacyEXComponent } from './advocacy-ex/advocacy-ex.component';
import { AttractionvisitedComponent } from './attractionvisited/attractionvisited.component';
import { BookingchannelsComponent } from './bookingchannels/bookingchannels.component';
import { InfrastructureComponent } from './infrastructure/infrastructure.component';
import { InfrastructureexComponent } from './infrastructureex/infrastructureex.component';
import { MedaihabitsComponent } from './medaihabits/medaihabits.component';
import { PilirsComponent } from './pilirs/pilirs.component';
import { PilirsexComponent } from './pilirsex/pilirsex.component';
import { PorposofvisitComponent } from './porposofvisit/porposofvisit.component';
import { RevisitComponent } from './revisit/revisit.component';
import { SatisfactionComponent } from './satisfaction/satisfaction.component';
import { SatisfactionexComponent } from './satisfactionex/satisfactionex.component';
import { SpendComponent } from './spend/spend.component';
import { VisitationbehaviorComponent } from './visitationbehavior/visitationbehavior.component';
import { VisitationdemographicComponent } from './visitationdemographic/visitationdemographic.component';
import { VisitorsurveyComponent } from './visitorsurvey/visitorsurvey.component';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'source-market' },
  {
    path: '',
    component: VisitorsurveyComponent,
    children: [
      { path: 'advocacy', component: AdvocacyComponent },
      { path: 'advocacyex', component: AdvocacyEXComponent },
      { path: 'attraction', component: AttractionvisitedComponent },
      { path: 'booking', component: BookingchannelsComponent },
      { path: 'infrastructure', component: InfrastructureComponent },
      { path: 'infrastructureex', component: InfrastructureexComponent },
      { path: 'pilirs', component: PilirsComponent },
      { path: 'pilirsex', component: PilirsexComponent },
      { path: 'porposofvisit', component: PorposofvisitComponent },
      { path: 'revisit', component: RevisitComponent },
      { path: 'satisfaction', component: SatisfactionComponent },
      { path: 'satisfactionex', component: SatisfactionexComponent },
      { path: 'spend', component: SpendComponent },
      { path: 'visitationbehavior', component: VisitationbehaviorComponent },
      { path: 'demographic', component: VisitationdemographicComponent },
      { path: 'media', component: MedaihabitsComponent },
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class VisitorsurveyRoutingModule { }

export const routedComponents = [
  AdvocacyComponent,
  AdvocacyEXComponent,
  AttractionvisitedComponent,
  BookingchannelsComponent,
  InfrastructureComponent,
  InfrastructureexComponent,
  MedaihabitsComponent,
  PilirsComponent,
  PilirsexComponent,
  PorposofvisitComponent,
  RevisitComponent,
  SatisfactionComponent,
  SatisfactionexComponent,
  SpendComponent,
  VisitationbehaviorComponent,
  VisitationdemographicComponent,
  VisitorsurveyComponent
]
