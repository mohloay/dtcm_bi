import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var InfrastructureExtBusLogic: any;
@Component({
  selector: 'InfrastructureExt',
  templateUrl: './infrastructureex.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class InfrastructureexComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      InfrastructureExtBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      InfrastructureExtBusLogic();
    }
  }
}
