import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfrastructureexComponent } from './infrastructureex.component';

describe('InfrastructureexComponent', () => {
  let component: InfrastructureexComponent;
  let fixture: ComponentFixture<InfrastructureexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfrastructureexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfrastructureexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
