import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SatisfactionexComponent } from './satisfactionex.component';

describe('SatisfactionexComponent', () => {
  let component: SatisfactionexComponent;
  let fixture: ComponentFixture<SatisfactionexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatisfactionexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SatisfactionexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
