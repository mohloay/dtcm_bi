import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var SatisfactionExtBusLogic: any;
@Component({
  selector: 'SatisfactionExt',
  templateUrl: './satisfactionex.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class SatisfactionexComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      SatisfactionExtBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      SatisfactionExtBusLogic();
    }
  }
}
