import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var BookingChannelsBusLogic: any;
@Component({
  selector: 'Booking',
  templateUrl: './bookingchannels.component.html',
  styleUrls: ['./bookingchannels.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class BookingchannelsComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      BookingChannelsBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      BookingChannelsBusLogic();
    }


  }

}
