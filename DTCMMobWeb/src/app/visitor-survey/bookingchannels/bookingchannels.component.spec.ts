import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingchannelsComponent } from './bookingchannels.component';

describe('BookingchannelsComponent', () => {
  let component: BookingchannelsComponent;
  let fixture: ComponentFixture<BookingchannelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingchannelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingchannelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
