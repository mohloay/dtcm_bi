import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvocacyEXComponent } from './advocacy-ex.component';

describe('AdvocacyEXComponent', () => {
  let component: AdvocacyEXComponent;
  let fixture: ComponentFixture<AdvocacyEXComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvocacyEXComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvocacyEXComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
