import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var AdvocacyExtBusLogic: any;
@Component({
  selector: 'AdvocacyExt',
  templateUrl: './advocacy-ex.component.html',
  styleUrls: ['./advocacy-ex.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class AdvocacyEXComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      AdvocacyExtBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      AdvocacyExtBusLogic();
    }


  }

}
