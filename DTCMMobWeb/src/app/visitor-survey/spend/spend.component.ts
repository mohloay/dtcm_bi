import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var SpendBusLogic: any;
@Component({
  selector: 'Spend',
  templateUrl: './spend.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class SpendComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      SpendBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      SpendBusLogic();
    }
  }
}
