import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var VisitationBehaviourBusLogic: any;
@Component({
  selector: 'Visitation',
  templateUrl: './visitationbehavior.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class VisitationbehaviorComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      VisitationBehaviourBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      VisitationBehaviourBusLogic();
    }
  }
}
