import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitationbehaviorComponent } from './visitationbehavior.component';

describe('VisitationbehaviorComponent', () => {
  let component: VisitationbehaviorComponent;
  let fixture: ComponentFixture<VisitationbehaviorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitationbehaviorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitationbehaviorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
