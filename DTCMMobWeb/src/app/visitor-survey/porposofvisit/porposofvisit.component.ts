import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var SegmentsBusLogic: any;
@Component({
  selector: 'PurposeOfVisit',
  templateUrl: './porposofvisit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class PorposofvisitComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      SegmentsBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      SegmentsBusLogic();
    }
  }
}
