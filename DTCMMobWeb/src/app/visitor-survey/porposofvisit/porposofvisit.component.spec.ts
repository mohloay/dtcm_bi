import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PorposofvisitComponent } from './porposofvisit.component';

describe('PorposofvisitComponent', () => {
  let component: PorposofvisitComponent;
  let fixture: ComponentFixture<PorposofvisitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PorposofvisitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PorposofvisitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
