import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PilirsComponent } from './pilirs.component';

describe('PilirsComponent', () => {
  let component: PilirsComponent;
  let fixture: ComponentFixture<PilirsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilirsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PilirsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
