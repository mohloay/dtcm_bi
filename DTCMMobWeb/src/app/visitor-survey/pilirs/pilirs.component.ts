import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var PillarsBusLogic: any;
@Component({
  selector: 'Pillars',
  templateUrl: './pilirs.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class PilirsComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      PillarsBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      PillarsBusLogic();
    }
  }
}
