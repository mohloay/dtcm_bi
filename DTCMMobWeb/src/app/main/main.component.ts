import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var MainBusLogic: any;
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class MainComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      MainBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      MainBusLogic();
    }

  }
}
