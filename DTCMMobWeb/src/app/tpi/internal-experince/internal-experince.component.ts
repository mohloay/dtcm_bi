import { Component, AfterViewInit , Injectable , ChangeDetectionStrategy } from '@angular/core';
declare var ExperienceIntrnlDashboard: any;
@Component({
  selector: 'InternalExperince',
  templateUrl: './internal-experince.component.html',
  styleUrls: ['./internal-experince.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class InternalExperinceComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
        console.clear();
        ExperienceIntrnlDashboard();
    }, 100);
    if (window["myqlik"] == null) {
        ExperienceIntrnlDashboard();
    }


}
}
