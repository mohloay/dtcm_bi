import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternalExperinceComponent } from './internal-experince.component';

describe('InternalExperinceComponent', () => {
  let component: InternalExperinceComponent;
  let fixture: ComponentFixture<InternalExperinceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternalExperinceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternalExperinceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
