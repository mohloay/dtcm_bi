import { Component, AfterViewInit , Injectable , ChangeDetectionStrategy } from '@angular/core';
declare var JourneyDashboard: any;
@Component({
  selector: 'Journey',
  templateUrl: './journy.component.html',
  styleUrls: ['./journy.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class JournyComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
        console.clear();
        JourneyDashboard();
    }, 100);
    if (window["myqlik"] == null) {
        JourneyDashboard();
    }


}

}
