import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { ExternalExperinceComponent } from './external-experince/external-experince.component';
import { InternalExperinceComponent } from './internal-experince/internal-experince.component';
import { JournyComponent } from './journy/journy.component';
import { OverviewComponent } from './overview/overview.component';
import { SourcemarketComponent } from './sourcemarket/sourcemarket.component';
import { VolumeComponent } from './volume/volume.component';
import { TpiComponent } from './tpi/tpi.component';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'source-market' },
  {
    path: '',
    component: TpiComponent, 
    children: [
      { path: 'sourcemarkettp', component: SourcemarketComponent },
      { path: 'overviewtp', component: OverviewComponent },
      { path: 'detail', component: DetailsComponent },
      { path: 'internalexperince', component: InternalExperinceComponent },
      { path: 'experinceexternal', component: ExternalExperinceComponent },
      { path: 'journy', component: JournyComponent },
      { path: 'volume', component: VolumeComponent }
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TpiRoutingModule { }

export const routedComponents = [
  DetailsComponent,
  ExternalExperinceComponent,
  InternalExperinceComponent,
  JournyComponent,
  OverviewComponent,
  SourcemarketComponent,
  VolumeComponent,
  TpiComponent
]
