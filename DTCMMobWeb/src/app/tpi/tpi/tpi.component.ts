import { Component, OnInit } from '@angular/core';
declare var TPINavBarFunctions: any;
@Component({
  selector: 'app-tpi',
  templateUrl: './tpi.component.html',
  styleUrls: ['./tpi.component.css']
})
export class TpiComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    TPINavBarFunctions();
  }

}
