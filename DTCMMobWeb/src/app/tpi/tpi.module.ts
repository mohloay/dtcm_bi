import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TpiRoutingModule, routedComponents   } from './tpi-routing.module';


@NgModule({
  declarations: [routedComponents],
  imports: [
    CommonModule ,
    TpiRoutingModule
  ]
})
export class TpiModule { }
