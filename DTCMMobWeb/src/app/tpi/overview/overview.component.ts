import { Component, AfterViewInit ,Injectable , ChangeDetectionStrategy } from '@angular/core';
declare var VolumeDashboard: any;
@Component({
  selector: 'overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class OverviewComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
        console.clear();
        VolumeDashboard();
    }, 100);
    if (window["myqlik"] == null) {
        VolumeDashboard();
    }


}
}
