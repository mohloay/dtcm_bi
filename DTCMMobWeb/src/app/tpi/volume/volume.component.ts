import { Component, AfterViewInit , Injectable , ChangeDetectionStrategy } from '@angular/core';
declare var VolumeDashboard: any;
@Component({
  selector: 'Volume',
  templateUrl: './volume.component.html',
  styleUrls: ['./volume.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VolumeComponent implements AfterViewInit  {

  constructor() { }

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
        console.clear();
        VolumeDashboard();
    }, 100);
    if (window["myqlik"] == null) {
        VolumeDashboard();
    }


}

}
