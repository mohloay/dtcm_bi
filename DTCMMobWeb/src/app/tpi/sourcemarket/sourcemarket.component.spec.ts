import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, AfterViewInit } from '@angular/core';
import { SourcemarketComponent  } from './sourcemarket.component';

describe('SourcemarketComponent', () => {
  let component: SourcemarketComponent;
  let fixture: ComponentFixture<SourcemarketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourcemarketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourcemarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
