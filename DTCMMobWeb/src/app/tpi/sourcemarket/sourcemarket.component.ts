import { Component, AfterViewInit , Injectable , ChangeDetectionStrategy } from '@angular/core';
declare var SoruceMarketDashboard : any;

@Component({
  selector: 'Source-Mareket',
  templateUrl: './sourcemarket.component.html',
  styleUrls: ['./sourcemarket.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class SourcemarketComponent implements AfterViewInit {
  constructor() { }
  
  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
        console.clear();
        SoruceMarketDashboard();
    }, 1000);
    if (window["myqlik"] == null) {
        SoruceMarketDashboard();
    }
   
      
}

}
