import { Component, AfterViewInit , Injectable , ChangeDetectionStrategy } from '@angular/core';
declare var DetailsDashBoard: any;
@Component({
  selector: 'Detials',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class DetailsComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
        console.clear();
        DetailsDashBoard();
    }, 100);
    if (window["myqlik"] == null) {
        DetailsDashBoard();
    }


}

}
