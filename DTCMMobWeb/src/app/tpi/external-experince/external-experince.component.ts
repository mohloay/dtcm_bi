import { Component, AfterViewInit , Injectable , ChangeDetectionStrategy} from '@angular/core';
declare var ExperienceExtrnlDashboard: any;
@Component({
  selector: 'ExternalExperince',
  templateUrl: './external-experince.component.html',
  styleUrls: ['./external-experince.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class ExternalExperinceComponent implements AfterViewInit {

  constructor() { }
  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
        console.clear();
        ExperienceExtrnlDashboard();
    }, 100);
    if (window["myqlik"] == null) {
        ExperienceExtrnlDashboard();
    }


}
}
