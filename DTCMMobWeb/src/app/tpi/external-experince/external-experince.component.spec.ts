import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalExperinceComponent } from './external-experince.component';

describe('ExternalExperinceComponent', () => {
  let component: ExternalExperinceComponent;
  let fixture: ComponentFixture<ExternalExperinceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalExperinceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalExperinceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
