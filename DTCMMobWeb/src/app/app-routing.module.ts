import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './main/main.component'

const routes: Routes = [
  { path: 'tpi', loadChildren: './tpi/tpi.module#TpiModule' },
  { path: 'overseas', loadChildren: './overseas/overseas.module#overseasModule' },
  { path: 'vs', loadChildren: './visitor-survey/visitor-survey.module#VisitorSurveyModule' },
  { path: 'brandtraker', loadChildren: './brandtraker/brandtraker.module#BrandtrakerModule' },
  { path: 'hotel', loadChildren: './hotel/hotel.module#HotelModule' },
  { path: 'main' , component : MainComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,{useHash:false})
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
