import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var BarriersBusLogic: any;
@Component({
  selector: 'Barriers',
  templateUrl: './barriers.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class BarriersComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      BarriersBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      BarriersBusLogic();
    }


  }
}
