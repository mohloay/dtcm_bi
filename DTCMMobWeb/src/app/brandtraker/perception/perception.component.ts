import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var PerceptionBusLogic: any;
@Component({
  selector: 'Perception',
  templateUrl: './perception.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})

@Injectable()
export class PerceptionComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      PerceptionBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      PerceptionBusLogic();
    }


  }
}
