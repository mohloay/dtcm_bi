import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var WishlistBusLogic: any;
@Component({
  selector: 'Wishlist',
  templateUrl: './wishlist.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})

@Injectable()
export class WishlistComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      WishlistBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      WishlistBusLogic();
    }


  }
}
