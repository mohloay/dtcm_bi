import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrandtrakerRoutingModule,routedComponents } from './brandtraker-routing.module'


@NgModule({
  declarations: [routedComponents],
  imports: [
    CommonModule,
    BrandtrakerRoutingModule
  ]
})
export class BrandtrakerModule { }

