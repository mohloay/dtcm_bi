import { Component, OnInit } from '@angular/core';
declare var BTNavBarFunctions: any;
@Component({
  selector: 'app-brandtracker',
  templateUrl: './brandtracker.component.html',
  styleUrls: ['./brandtracker.component.css']
})
export class BrandtrackerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    BTNavBarFunctions();
  }

}
