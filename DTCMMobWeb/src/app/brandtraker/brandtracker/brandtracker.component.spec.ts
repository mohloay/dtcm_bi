import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandtrackerComponent } from './brandtracker.component';

describe('BrandtrackerComponent', () => {
  let component: BrandtrackerComponent;
  let fixture: ComponentFixture<BrandtrackerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandtrackerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandtrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
