import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var AdvertisingBusLogic: any;
@Component({
  selector: 'Advertising',
  templateUrl: './brand-adver.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class BrandAdverComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      AdvertisingBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      AdvertisingBusLogic();
    }


  }
}
