import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandAdverComponent } from './brand-adver.component';

describe('BrandAdverComponent', () => {
  let component: BrandAdverComponent;
  let fixture: ComponentFixture<BrandAdverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandAdverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandAdverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
