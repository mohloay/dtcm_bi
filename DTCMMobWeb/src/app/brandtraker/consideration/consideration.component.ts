import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var ConsiderationBusLogic: any;
@Component({
  selector: 'Consideration',
  templateUrl: './consideration.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class ConsiderationComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      ConsiderationBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      ConsiderationBusLogic();
    }


  }
}
