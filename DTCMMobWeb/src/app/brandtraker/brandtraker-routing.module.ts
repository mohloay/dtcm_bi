import { NgModule } from '@angular/core';
import { BarriersComponent } from './barriers/barriers.component';
import { BrandAdverComponent } from './brand-adver/brand-adver.component';
import { ConsiderationComponent } from './consideration/consideration.component';
import { PerceptionComponent } from './perception/perception.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { BrandtrackerComponent } from './brandtracker/brandtracker.component';
import { Routes, RouterModule } from '@angular/router';


export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'barriers' },
  {
    path: '',
    component: BrandtrackerComponent,
    children: [
      { path: 'barriers', component: BarriersComponent },
      { path: 'brandAdver', component: BrandAdverComponent },
      { path: 'consideration', component: ConsiderationComponent },
      { path: 'perception', component: PerceptionComponent },
      { path: 'wishlist', component: WishlistComponent },
      
    ]
  }
]


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class BrandtrakerRoutingModule { }


export const routedComponents = [
  BarriersComponent,
  BrandAdverComponent,
  ConsiderationComponent,
  PerceptionComponent,
  WishlistComponent,
  BrandtrackerComponent
]
