import { Component , AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var GuestsDashBusLogic: any;
@Component({
  selector: 'Guests',
  templateUrl: './guests.component.html',
  styleUrls: ['./guests.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

@Injectable()
export class GuestsComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
        console.clear();
        GuestsDashBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
        GuestsDashBusLogic();
    }


}

}
