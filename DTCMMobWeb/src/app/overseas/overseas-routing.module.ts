import { NgModule } from '@angular/core';
import {CapacityComponent}from './capacity/capacity.component';
import {GuestsComponent} from'./guests/guests.component';
import {LenghtofStayComponent} from './lenghtof-stay/lenghtof-stay.component';
import {OverseasComponent} from './overseas/overseas.component'
import {OverviewComponent } from './overview/overview.component';
import {SourcemarketComponent} from './sourcemarket/sourcemarket.component';
import { from } from 'rxjs';
import { Routes, RouterModule } from '@angular/router';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'sourcemarketos' },
  {
    path: '',
    component: OverseasComponent, 
    children: [
      { path: 'sourcemarketos', component: SourcemarketComponent },
      { path: 'overviewos', component: OverviewComponent },
      { path: 'capacity', component: CapacityComponent },
      { path: 'guests', component: GuestsComponent },
      { path: 'lenghtofstay', component: LenghtofStayComponent }
     
    ]
  }
]


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class OverseasRoutingModule { }


export const routedComponents = [
  CapacityComponent,
  LenghtofStayComponent,
  OverviewComponent,
  SourcemarketComponent,
  GuestsComponent,
  OverseasComponent
]
