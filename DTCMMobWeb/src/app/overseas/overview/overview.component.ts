import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var OverviewDashBusLogic: any;
@Component({
  selector: 'OverViewOverseas',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class OverviewComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit(): void {

    setTimeout(() => {    //<<<---    using ()=> syntax
        console.clear();
        OverviewDashBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
        OverviewDashBusLogic();
    }
}

}
