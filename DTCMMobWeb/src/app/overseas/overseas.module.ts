import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {routedComponents , OverseasRoutingModule}from './overseas-routing.module'


@NgModule({
  declarations: [routedComponents],
  imports: [
    CommonModule,
    OverseasRoutingModule
  ]
})


export class overseasModule { }
