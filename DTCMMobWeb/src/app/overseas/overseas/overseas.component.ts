import { Component, OnInit } from '@angular/core';
declare var OSNavbarFunctions: any;
@Component({
  selector: 'app-overseas',
  templateUrl: './overseas.component.html',
  styleUrls: ['./overseas.component.css']
})
export class OverseasComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    OSNavbarFunctions();
  }

}
