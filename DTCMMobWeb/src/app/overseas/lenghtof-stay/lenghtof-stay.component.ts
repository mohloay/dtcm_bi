import { Component,  AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var LengthofstayDashBusLogic: any;
@Component({
  selector: 'LenghtOFStay',
  templateUrl: './lenghtof-stay.component.html',
  styleUrls: ['./lenghtof-stay.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class LenghtofStayComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
        console.clear();
        LengthofstayDashBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
        LengthofstayDashBusLogic();
    }


}

}
