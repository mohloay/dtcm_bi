import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LenghtofStayComponent } from './lenghtof-stay.component';

describe('LenghtofStayComponent', () => {
  let component: LenghtofStayComponent;
  let fixture: ComponentFixture<LenghtofStayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LenghtofStayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LenghtofStayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
