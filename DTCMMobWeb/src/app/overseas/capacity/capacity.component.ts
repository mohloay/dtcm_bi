import { Component,  AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var SeatcapacityDashboardBusLogic: any;
@Component({
  selector: 'SetCapacity',
  templateUrl: './capacity.component.html',
  styleUrls: ['./capacity.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class CapacityComponent implements AfterViewInit {

  constructor() { }

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
        console.clear();
        SeatcapacityDashboardBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
        SeatcapacityDashboardBusLogic();
    }


}

}
