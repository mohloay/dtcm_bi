import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PipleComponent } from './piple.component';

describe('PipleComponent', () => {
  let component: PipleComponent;
  let fixture: ComponentFixture<PipleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PipleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
