import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var PipelineBusLogic: any;
@Component({
  selector: 'Pipeline',
  templateUrl: './piple.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class PipleComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      PipelineBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      PipelineBusLogic();
    }


  }
}
