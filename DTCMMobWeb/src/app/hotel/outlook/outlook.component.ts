import { Component, AfterViewInit, Injectable, ChangeDetectionStrategy } from '@angular/core';
declare var OutlookBusLogic: any;
@Component({
  selector: 'Outlook',
  templateUrl: './outlook.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
@Injectable()
export class OutlookComponent implements AfterViewInit {

  ngAfterViewInit(): void {
    setTimeout(() => {    //<<<---    using ()=> syntax
      console.clear();
      OutlookBusLogic();
    }, 1000);
    if (window["myqlik"] == null) {
      OutlookBusLogic();
    }


  }
}
