import { Component, OnInit } from '@angular/core';
declare var HotelNavBarFunctions: any;
@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    HotelNavBarFunctions();
  }

}
