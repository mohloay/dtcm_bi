import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {routedComponents ,HotelRoutingModule}from './hotel-routing.module'
import { from } from 'rxjs';

@NgModule({
  declarations: [routedComponents],
  imports: [
    CommonModule , 
    HotelRoutingModule
  ]
})
export class HotelModule { }
