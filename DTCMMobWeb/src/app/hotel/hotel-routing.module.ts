import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetialsComponent } from './detials/detials.component';
import { OutlookComponent } from './outlook/outlook.component';
import { PipleComponent } from './piple/piple.component';
import { HotelComponent } from './hotel/hotel.component';


export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'detials' },
  {
    path: '',
    component: HotelComponent,
    children: [
      { path: 'detials', component: DetialsComponent },
      { path: 'outlook', component: OutlookComponent },
      { path: 'piple', component: PipleComponent }
    ]
  }
]
@NgModule({
 
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HotelRoutingModule { }
export const routedComponents = [
  DetialsComponent, OutlookComponent, PipleComponent, HotelComponent
]
