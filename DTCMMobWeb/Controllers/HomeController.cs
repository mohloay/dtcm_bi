using DTCMMobWeb.Context;
using DTCMMobWeb.Generic_Function;
using DTCMMobWeb.Models;
using DTCMWebMobile.Generic_Function;
using Qlik.Engine;
using Qlik.Sense.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.SessionState;

namespace DTCMWebMobile.Controllers
{

  
    public class HomeController : Controller
    {
     // UserPNToken
    dashboard_metadataEntities dTCMDBEntities = new dashboard_metadataEntities();
    // DTCMDBEntities dTCMDBEntities = new DTCMDBEntities();
  //  DTCMEntities dTCMDBEntities = new DTCMEntities();
        public ActionResult Index(string Ticket = null)
        {
            //if (Session["QlickTicket"] == null && Ticket == null)
            //{
            //    return RedirectToAction("Login", "Account");
            //}
            //if (Session["QlickTicket"] != null)
            //    ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            //else if (Ticket != null)
            //{

            //    Session["QlickTicket"] = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
            //    ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            //}
            //ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
          
            return View();
        }
    public ActionResult DTCM(string Ticket = null)
    {
      var Tickets = Request.Params["Ticket"];
      if (Session["QlickTicket"] == null && Ticket == null)
      {
        return RedirectToAction("Login", "Account");
      }
      if (Session["QlickTicket"] != null)
        ViewBag.QlickTicket = Session["QlickTicket"].ToString();
      else if (Ticket != null)
      {
        var UserName1 = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username;
        string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
        ViewBag.QlickTicket = ticket;
        Session["QlickTicket"] = ticket;
      }
      ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
      ViewBag.TPIDashApp = WebConfigurationManager.AppSettings["TPIDashAPPValue"].ToString();
      ViewBag.OverseasDashApp = WebConfigurationManager.AppSettings["OverSeasDashAPPValue"].ToString();
      ViewBag.DIVDashApp = WebConfigurationManager.AppSettings["DivsvDashAPPValue"].ToString();
      ViewBag.BrandTrakerDashAppValue = WebConfigurationManager.AppSettings["BrandTrakerDashAppValue"].ToString();
      ViewBag.HotelPipeLineDashApp = WebConfigurationManager.AppSettings["HotelPipeLineDashAPPValue"].ToString();
      ViewBag.MainDashApp = WebConfigurationManager.AppSettings["MobAppMain"].ToString();

      ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();

      return View();
    }

    public ActionResult TPIDashBord(string Ticket =null)
        {

            if (Session["QlickTicket"] == null && Ticket == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["QlickTicket"] != null)
                ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            else if (Ticket != null)
            {
                string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
                ViewBag.QlickTicket = ticket;
                Session["QlickTicket"] = ticket;
            }
            ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
            ViewBag.TPIDashApp = WebConfigurationManager.AppSettings["TPIDashAPPValue"].ToString();
           
           
            ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();
            return View();
        }


        public ActionResult NEWTPIDashBord(string Ticket = null)
        {

            if (Session["QlickTicket"] == null && Ticket == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["QlickTicket"] != null)
                ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            else if (Ticket != null)
            {
                string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
                ViewBag.QlickTicket = ticket;
                Session["QlickTicket"] = ticket;
            }
            ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
            ViewBag.TPIDashApp = WebConfigurationManager.AppSettings["TPIDashAPPValue"].ToString();
           
            ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();
            return View();
        }

        public ActionResult PricingDashboard(string Ticket = null)
        {
            if (Session["QlickTicket"] == null && Ticket == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["QlickTicket"] != null)
                ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            else if (Ticket != null)
            {
                string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
                ViewBag.QlickTicket = ticket;
                Session["QlickTicket"] = ticket;
            }

            ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
            ViewBag.PricingDashApp = WebConfigurationManager.AppSettings["PricingDashApp"].ToString();
          
           
            ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();
            return View();
        }
        
        public ActionResult NewPricingDashboard(string Ticket = null)
        {
            if (Session["QlickTicket"] == null && Ticket == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["QlickTicket"] != null)
                ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            else if (Ticket != null)
            {
                string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
                ViewBag.QlickTicket = ticket;
                Session["QlickTicket"] = ticket;
            }
            ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
            ViewBag.PricingDashApp = WebConfigurationManager.AppSettings["PricingDashApp"].ToString();

          
            
            ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();
            return View();
        }
        public ActionResult BrandTracker(string Ticket = null)
        {
            if (Session["QlickTicket"] == null && Ticket == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["QlickTicket"] != null)
                ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            else if (Ticket != null)
            {
                string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
                ViewBag.QlickTicket = ticket;
                Session["QlickTicket"] = ticket;
            }
            ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
            ViewBag.BrandTrakerDashAppValue = WebConfigurationManager.AppSettings["BrandTrakerDashAppValue"].ToString();
            ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();
            return View();
        }

        public ActionResult NewBrandTracker(string Ticket = null)
        {
            if (Session["QlickTicket"] == null && Ticket == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["QlickTicket"] != null)
                ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            else if (Ticket != null)
            {
                string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
                ViewBag.QlickTicket = ticket;
                Session["QlickTicket"] = ticket;
            }
            ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
            ViewBag.BrandTrakerDashAppValue = WebConfigurationManager.AppSettings["BrandTrakerDashAppValue"].ToString();
           
            
            ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();
            return View();
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult OverSeas(string Ticket =null)
        {
            if (Session["QlickTicket"] == null && Ticket == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["QlickTicket"] != null)
                ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            else if (Ticket != null)
            {
                string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
                ViewBag.QlickTicket = ticket;
                Session["QlickTicket"] = ticket;
            }
            ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
            ViewBag.OverseasDashApp = WebConfigurationManager.AppSettings["OverSeasDashAPPValue"].ToString();
            ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();
            return View();
        }
        public ActionResult NewOverSeas(string Ticket = null)
        {
            if (Session["QlickTicket"] == null && Ticket == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["QlickTicket"] != null)
                ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            else if (Ticket != null)
            {
                string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
                ViewBag.QlickTicket = ticket;
                Session["QlickTicket"] = ticket;
            }
            ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
            ViewBag.OverseasDashApp = WebConfigurationManager.AppSettings["OverSeasDashAPPValue"].ToString();
           
           
            ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();
            return View();
        }

        public ActionResult HotelPipeLine (string Ticket = null)
        {
            if (Session["QlickTicket"] == null && Ticket == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["QlickTicket"] != null)
                ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            else if (Ticket != null)
            {
                string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
                ViewBag.QlickTicket = ticket;
                Session["QlickTicket"] = ticket;
            }
            ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
            ViewBag.HotelPipeLineDashApp = WebConfigurationManager.AppSettings["HotelPipeLineDashAPPValue"].ToString();
            ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();
            return View();
        }

        public ActionResult NewHotelPipeLine(string Ticket = null)
        {
            if (Session["QlickTicket"] == null && Ticket == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["QlickTicket"] != null)
                ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            else if (Ticket != null)
            {
                string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
                ViewBag.QlickTicket = ticket;
                Session["QlickTicket"] = ticket;
            }
            ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
            ViewBag.HotelPipeLineDashApp = WebConfigurationManager.AppSettings["HotelPipeLineDashAPPValue"].ToString();
           
            
            ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();
            return View();
        }

        public ActionResult DIVDashbord(string Ticket = null)
        {
            if (Session["QlickTicket"] == null && Ticket == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["QlickTicket"] != null)
                ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            else if (Ticket != null)
            {
                string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
                ViewBag.QlickTicket = ticket;
                Session["QlickTicket"] = ticket;
            }
            ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
            ViewBag.DIVDashApp = WebConfigurationManager.AppSettings["DivsvDashAPPValue"].ToString();
            ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();
            return View();
        }

        public ActionResult VSDashbord(string Ticket = null)
        {
            if (Session["QlickTicket"] == null && Ticket == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["QlickTicket"] != null)
                ViewBag.QlickTicket = Session["QlickTicket"].ToString();
            else if (Ticket != null)
            {
                string ticket = UserFunction.GetTicket(new Models.UserLogin() { UserName = dTCMDBEntities.Users.FirstOrDefault(user => user.UserToken == Ticket).Username });
                ViewBag.QlickTicket = ticket;
                Session["QlickTicket"] = ticket;
            }
            ViewBag.Qlikserver = (WebConfigurationManager.AppSettings["QlikServerIP"].ToString()).Split(new string[] { "//" }, StringSplitOptions.None)[1];
            ViewBag.DIVDashApp = WebConfigurationManager.AppSettings["DivsvDashAPPValue"].ToString();
           
            
            ViewBag.StopSelectionAndInteractionsCode = WebConfigurationManager.AppSettings["SelectionAndInteractionsCode"].ToString();
            return View();
        }

        [HttpGet]
        public ActionResult SendNotification()
        {
            if (Session["GroupsSendNotifications"] != null)
            {
                SendNotifacationModel sendNotifacationModel = new SendNotifacationModel();
                //get the Current Groups 
                var ListGroup = dTCMDBEntities.Groups.ToList();
                List<GroupSelect> listGr = new List<GroupSelect>();
                foreach (var Group in ListGroup)
                {
                    listGr.Add(new GroupSelect() { GroupName = Group.Name });
                }
                sendNotifacationModel.listGroup = listGr;
                //get the Current System Users 
                var ListUser = dTCMDBEntities.Users.ToList();
                List<UserSelect> listus = new List<UserSelect>();
                foreach (var UserPor in ListUser)
                {
                    listus.Add(new UserSelect() { UserName = UserPor.Username, Fullname = UserPor.Fullname, Email = UserPor.Email, Organization = UserPor.Organization });
                }
                sendNotifacationModel.listUser = listus;
                return View(sendNotifacationModel);
            }else
                return RedirectToAction("NewLogin", "Account");
        }
        [HttpPost]
        public ActionResult SendNotification(SendNotifacationModel model)
        {
            if (Session["GroupsSendNotifications"] != null)
            {
                if (ModelState.IsValid)
                {
                    List<string> listSelectedGroups = model.listGroup.Where(Mo => Mo.IsSelected == true).Select(s => s.GroupName).ToList();
                    List<string> listSelectedUsers = model.listUser.Where(Mo => Mo.IsSelected == true).Select(s => s.UserName).ToList();
                    List<int> UserIDs = (from US in dTCMDBEntities.Users
                                         from Gr in dTCMDBEntities.Groups
                                         where listSelectedGroups.Contains(Gr.Name) || listSelectedUsers.Contains(US.Username)
                                         select US.ID).Distinct().ToList();
                    List<string> UserIDsToken = (from Us in dTCMDBEntities.Users
                                                 join UsTo in dTCMDBEntities.UserPNTokens on Us.ID equals UsTo.UserID
                                                 where (UserIDs.Contains(Us.ID))
                                                 select UsTo.PNToken).ToList();
                    foreach (var UserToken in UserIDsToken)
                    {
                        UserFunction.SendNofticationMessage(model.Title, model.Message, UserToken);
                    }
                }
                return View(model);
            }
            return RedirectToAction("Newlogin", "Account");
        }
        public ActionResult UploadImages()
        {
            if (Session["GroupsUploadImages"] != null)
            {
                var listOfNames = dTCMDBEntities.Names.Select(s => s.Name1).ToList();
                return View(listOfNames);
            }else
                return RedirectToAction("NewLogin", "Account");
        }

        public PartialViewResult GetImagesForName(string ObjName)
        {
            var nameobject = dTCMDBEntities.Names.FirstOrDefault(x => x.Name1 == ObjName);
            List<ImageModel> ListImage = new List<ImageModel>();
            if(nameobject != null)
            {
                var List = dTCMDBEntities.NameImages.Where(s => s.NameID == nameobject.ID).ToList();
                foreach (var item in List)
                {
                    ListImage.Add(new ImageModel() {Name= ObjName, URL=item.NameOfImage , ID = item.ID});
                }
            }
            int NumberOFImage = Convert.ToInt32(WebConfigurationManager.AppSettings["NumberOFImg"].ToString());
            ViewBag.ObjName = ObjName;
            if (ListImage.Count >= NumberOFImage)
                ViewBag.ShowAdd = false;
            else
                ViewBag.ShowAdd = true;
            ViewBag.ObjName = ObjName;
            ViewBag.NoteDescription = nameobject.NameDescription;
            return PartialView("_DetailImagView", ListImage);
        }
        public PartialViewResult DeleteImagesForName(int ID , string ObjName)
        {
           
            List<ImageModel> ListImage = new List<ImageModel>();
            var  Name = dTCMDBEntities.NameImages.Where(s => s.ID == ID).FirstOrDefault();
            string filePath = Server.MapPath("~/" + Name.NameOfImage);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
            int? CurrentNameID = Name.NameID;
            dTCMDBEntities.NameImages.Remove(Name);
            dTCMDBEntities.SaveChanges();
            var List = dTCMDBEntities.NameImages.Where(s => s.NameID == CurrentNameID).ToList();
            foreach (var item in List)
            {
                ListImage.Add(new ImageModel() { Name = ObjName, URL = item.NameOfImage, ID = item.ID });
            }
            int NumberOFImage = Convert.ToInt32(WebConfigurationManager.AppSettings["NumberOFImg"].ToString());
            ViewBag.ObjName = ObjName;
            if (ListImage.Count >= NumberOFImage)
                ViewBag.ShowAdd = false;
            else
                ViewBag.ShowAdd = true;
            return PartialView("_DetailImagView", ListImage);
        }
        
        public PartialViewResult AddImageForName(UploadImageModal uploadImageModal)
        {
            List<ImageModel> ListImage = new List<ImageModel>();
            if (uploadImageModal.file !=null)
            {
                try
                {
                    string FileName = Path.GetFileNameWithoutExtension(uploadImageModal.file.FileName);
                    string Extention = Path.GetExtension(uploadImageModal.file.FileName);
                    FileName = FileName + DateTime.Now.ToString("dd-mm-yyy") + Extention;
                    uploadImageModal.file.SaveAs(Server.MapPath("/images/UploadedImages/" + FileName));
                    int CurrentObjectID = dTCMDBEntities.Names.FirstOrDefault(x => x.Name1 == uploadImageModal.Name).ID;
                    dTCMDBEntities.NameImages.Add(new NameImage() { NameID = CurrentObjectID, NameOfImage = "/images/UploadedImages/" + FileName });
                    dTCMDBEntities.SaveChanges();
                    var List = dTCMDBEntities.NameImages.Where(s => s.NameID == CurrentObjectID).ToList();
                    foreach (var item in List)
                    {
                        ListImage.Add(new ImageModel() { Name = uploadImageModal.Name, URL = item.NameOfImage, ID = item.ID });
                    }
                }catch(Exception ex)
                {
                    UserFunction.LogError("Add Image For Name ", ex);
                }
            }
            int NumberOFImage = Convert.ToInt32(WebConfigurationManager.AppSettings["NumberOFImg"].ToString());
            ViewBag.ObjName = uploadImageModal.Name;
            if (ListImage.Count >= NumberOFImage)
                ViewBag.ShowAdd = false;
            else
                ViewBag.ShowAdd = true;
            return PartialView("_DetailImagView", ListImage);
        }
    }
}
