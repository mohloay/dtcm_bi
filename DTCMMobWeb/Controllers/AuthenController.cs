using DTCMMobWeb.Context;
using DTCMMobWeb.Generic_Function;
using DTCMMobWeb.Models;
using DTCMWebMobile.Generic_Function;
using DTCMWebMobile.Models;
using Newtonsoft.Json;
using Qlik.Engine;
using Qlik.Sense.Client;
using Qlik.Sense.Client.Visualizations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebSocketSharp;

namespace DTCMMobWeb.Controllers
{
    [RoutePrefix("api/Authen")]
    public class AuthenController : ApiController
    {


        // GET: api/Authen

        [HttpPost]
        public ReturnType GetTicket(UserLogin _UserLogin)
        {
      //dashboard_metadataEntities dashboard_metadataEntities = new dashboard_metadataEntities();
      dashboard_metadataEntities dashboard_metadataEntities = new dashboard_metadataEntities();
      UserLogin model = new UserLogin() { UserName = _UserLogin.UserName, Password = _UserLogin.Password };
            ReturnType ReturnValues = new ReturnType();
            try
            {
                //UserLogin model = new UserLogin() { UserName = _UserLogin.UserName, Password = _UserLogin.Password };
                ReturnLogingMobileClass returnLogingMobileClass = UserFunction.CheckMobileUSer(model);
                if (returnLogingMobileClass.CheckUser)
                    {
                    ReturnValues.IsAthen = true;
                    #region Qlik sense 
                    
                    var uri = new Uri(WebConfigurationManager.AppSettings["QlikServerIP"].ToString());
                    ILocation location = Qlik.Engine.Location.FromUri(new Uri(WebConfigurationManager.AppSettings["QlikServerIP"]));
                  
                    var domain = WebConfigurationManager.AppSettings["DomainQlikDir"];
                    string user = model.UserName;
                    SecureString pwd = new SecureString();
                    foreach (var chr in model.Password)
                    {
                        pwd.AppendChar(chr);
                    }
                    location.VirtualProxyPath = WebConfigurationManager.AppSettings["CentralProxy"];
                    location.AsNtlmUserViaProxy(proxyUsesSsl: uri.Scheme.Equals(Uri.UriSchemeHttps), loginCredentials: new NetworkCredential(user, pwd, domain), certificateValidation: false);
                    location.IsVersionCheckActive = false;
                    var ListApps = location.GetAppIdentifiers();
                    try
                    {
                        foreach (IAppIdentifier appIdentifier in ListApps)
                        {
                            AppUser appUser = new AppUser();
                            appUser.Name = appIdentifier.AppName;
                            var Dashbord = dashboard_metadataEntities.Dashboards.FirstOrDefault(Dch => Dch.name == appIdentifier.AppName && Dch.Show == true);
                            if (Dashbord != null)
                            {
                                using (var app = location.App(appIdentifier))// Location specified in Accessing
                                {
                                    var MasterObjectList = app.GetMasterObjectList();
                                    var MasterObjectListItem = MasterObjectList.Items;
                                    ISheetList sheetList = app.GetSheetList();
                                    // get the List with perivalage objects

                                    MetaData metaData = new MetaData()
                                    {
                                        description = Dashbord.description,
                                        icon = Dashbord.icon,
                                        image = Dashbord.image,
                                        name = Dashbord.name,
                                        namear = Dashbord.namear,
                                        nameen = Dashbord.nameen,
                                        order = Dashbord.order
                                    };
                                    foreach (ISheetObjectViewListContainer item in sheetList.Items)
                                    {
                                       
                                        var page = dashboard_metadataEntities.Pages.Where(D => D.DashboardID == Dashbord.ID && D.name == item.Data.Title.Trim() && D.Show == true).FirstOrDefault();
                                        if (page != null)
                                        {
                                            PageModel pageModel = new PageModel()
                                            {
                                                description = page.description,
                                                url = page.url,
                                                icon = page.icon,
                                                name = page.name,
                                                DashboardID = page.DashboardID,
                                                order = page.order,
                                                namear = page.namear,
                                                nameen = page.nameen,
                                                FullNameAr = page.FullNameAr,
                                                FullNameEn = page.FullNameEn,
                                                RLID = page.RLId
                                            };
                                            pageModel.objects = SecurtyObject(dashboard_metadataEntities.Objects.Where(obj => obj.PageID == page.ID).ToList(), page.ID, MasterObjectListItem);
                                            metaData.pages.Add(pageModel);
                                            appUser.Pages.Add(item.Data.Title);
                                        }
                                    }
                                    ReturnValues.ListAppWithPagesWithUserObjects.Add(metaData);
                                }
                            }
                        }
                        if (ReturnValues.ListAppWithPagesWithUserObjects.Count == 0)
                        {
                            ReturnValues.ErrorMessage = "You don't have access to dashboard ";
                        }

                    }
                    catch (Exception ex)
                    {
                        ReturnValues.ErrorMessage = "There is an error occurred on dashboard server please try later ";
                        UserFunction.LogError("Qlik sense Error", ex);
                    }
                    #endregion

                    #region Database
                    ReturnValues.TicketValue = UserFunction.GenerateToken(model.UserName, model.Password);//UserFunction.GetTicket(model);
                    Context.User Cuser = new Context.User()
                    {
                        Email = returnLogingMobileClass.Email,
                        Organization = returnLogingMobileClass.Organization,
                        Username = model.UserName,
                        Fullname = returnLogingMobileClass.FullName,
                        UserToken = ReturnValues.TicketValue,
                        CreationDate = DateTime.Now
                    };
                    var Currentuser = dashboard_metadataEntities.Users.FirstOrDefault(user1 => user1.Username == model.UserName);
                    if (Currentuser !=null)
                    {
                        Currentuser.Email = returnLogingMobileClass.Email;
                        Currentuser.Organization = returnLogingMobileClass.Organization;
                        Currentuser.Username = model.UserName;
                        Currentuser.Fullname = returnLogingMobileClass.FullName;
                        Currentuser.UserToken = ReturnValues.TicketValue;
                        Currentuser.CreationDate = DateTime.Now;
                    }
                    else
                    {
                        dashboard_metadataEntities.Users.Add(Cuser);
                    }
                    foreach (var item in Cuser.Groups)
                    {
                        Cuser.Groups.Remove(item);
                    }
                    dashboard_metadataEntities.SaveChanges();
                    

                    // add Groups to the Database
                    foreach (var Group in returnLogingMobileClass.listGroups)
                    {
                        var CurrentGroup = dashboard_metadataEntities.Groups.Where(Gr => Gr.Name == Group).FirstOrDefault();
                        if (CurrentGroup ==null)
                        {
                            Group group = new Context.Group() { Name = Group };
                            dashboard_metadataEntities.Groups.Add(group);
                            dashboard_metadataEntities.SaveChanges();
                            Cuser.Groups.Add(group);
                        }
                        else
                        {
                            Cuser.Groups.Add(CurrentGroup);
                        }
                    }
                    dashboard_metadataEntities.SaveChanges();
                    
                    ReturnValues.AllowSnap = returnLogingMobileClass.AllowSnap;
                    ReturnValues.FullName = returnLogingMobileClass.FullName;

                }
                else
                {
                    ReturnValues.IsAthen = false;
                    ReturnValues.ErrorMessage = returnLogingMobileClass.ErorrMessage;
                }
                #endregion
            }
            catch (Exception Ex)
            {
                ReturnValues.ErrorMessage = "There is an error occurred please try later";
                UserFunction.LogError("Qlik sense Error", Ex);
            }
            return ReturnValues;

        }

        [HttpGet]
        public CurrentObjectPorpertiesItem GetMobileViewObject(int PageID)
        {
            dashboard_metadataEntities dashboard_metadataEntities = new dashboard_metadataEntities();

            CurrentObjectPorpertiesItem _currentObjectPorpertiesItem = new CurrentObjectPorpertiesItem();
            try
            {
                _currentObjectPorpertiesItem.CurrentObjectListShow = dashboard_metadataEntities.Objects.Where(page => page.PageID == PageID).ToList();
            }catch(Exception ex)
            {
                UserFunction.LogError("error in get from the database ", ex);
            }
            return _currentObjectPorpertiesItem;
        }

        private List<string> SecurtyObject(List<Context.Object> listObject, int PageID, IEnumerable<MasterObjectObjectViewListContainer> Items)
        {
            dashboard_metadataEntities dashboard_metadataEntities = new dashboard_metadataEntities();
            if (Items.ToList().Count > 0)
            {
                Page page = dashboard_metadataEntities.Pages.FirstOrDefault(pa => pa.ID == PageID);
                foreach (var item in Items)
                {

                    switch (page.name.ToString())
                    {
                        case "Details":

                            if (item.Data.Title == "TPI MI Residence PCT KPI")
                            {
                                listObject.FirstOrDefault(obj => obj.ID == "detail_residence_obj").PrivilegeShow = true;
                            }
                            else if (item.Data.Title == "TPI MI Hotel Hopping KPI")
                            {
                                listObject.FirstOrDefault(obj => obj.ID == "detail_hh_obj").PrivilegeShow = true;
                            }
                            break;
                        case "Journey":

                            if (item.Data.Title == "TPI MI Media Landscape")
                            {
                                listObject.FirstOrDefault(obj => obj.ID == "journey_media_landscape_obj").PrivilegeShow = true;
                            }
                            else if (item.Data.Title == "TPI MI Booking Method")
                            {
                                listObject.FirstOrDefault(obj => obj.ID == "journey_booking_method_obj").PrivilegeShow = true;
                            }
                            else if (
                               item.Data.Title == "TPI MI TS Key Travel Period" || item.Data.Title == "TPI MI TS School Term 1" ||
                               item.Data.Title == "TPI MI TS School Term 2" || item.Data.Title == "TPI MI TS School Term 1" ||
                               item.Data.Title == "TPI MI TS School Term 4" || item.Data.Title == "TPI MI TS Yearly Vacation Days"
                               )
                            {
                                listObject.FirstOrDefault(obj => obj.ID == "journey_travel_seasons_obj").PrivilegeShow = true;
                            }
                            break;
                        case "Length of Stay":

                            if (item.Data.Title == "Overseas MI PCT Residence by SM (YTD)")
                            {
                                listObject.FirstOrDefault(obj => obj.ID == "resident_pct_obj").PrivilegeShow = true;
                            }

                            break;
                        case "Overview":

                            if (item.Data.Title == "Overseas MI Seat Capacity to ME CY KPI" || item.Data.Title == "Overseas MI Seat Capacity to ME PY KPI"
                               || item.Data.Title == "Overseas MI Seat Capacity to ME YoY KPI")
                            {
                                listObject.FirstOrDefault(obj => obj.ID == "overview_seat_capacity_to_me_obj").PrivilegeShow = true;
                            }
                            else if (item.Data.Title == "Overseas MI Seat Capacity to World CY KPI" || item.Data.Title == "Overseas MI Seat Capacity to World PY KPI"
                              || item.Data.Title == "Overseas MI Seat Capacity to World YoY KPI")
                            {
                                listObject.FirstOrDefault(obj => obj.ID == "overview_seat_capacity_to_world_obj").PrivilegeShow = true;
                            }

                            break;

                        default:
                            break;
                    }

                }

            }
            listObject.RemoveAll(ob => ob.PrivilegeShow == false);
            return listObject.Select(s => s.NameEN).ToList();

        }

        [HttpGet]
        [AuthAttr]
        public List<MetaData> GetMetaData()
        {
            dashboard_metadataEntities dashboard_metadataEntities = new dashboard_metadataEntities();
            List<MetaData> metaDataList = new List<MetaData>();
            foreach (var Dashbord in dashboard_metadataEntities.Dashboards.ToList())
            {
                MetaData metaData = new MetaData()
                {
                    description = Dashbord.description,
                    icon = Dashbord.icon,
                    image = Dashbord.image,
                    name = Dashbord.name,
                    namear = Dashbord.namear,
                    nameen = Dashbord.nameen,
                    order = Dashbord.order
                };
                foreach (var page in dashboard_metadataEntities.Pages.Where(D => D.DashboardID == Dashbord.ID).ToList())
                {
                    PageModel pageModel = new PageModel()
                    {
                        description = page.description,
                        url = page.url,
                        icon = page.icon,
                        name = page.name,
                        DashboardID = page.DashboardID,
                        order = page.order,
                        namear = page.namear,
                        nameen = page.nameen
                    };
                    foreach (var Obj in dashboard_metadataEntities.Objects.Where(obK => obK.PageID == page.ID && obK.Show == true).ToList())
                    {
                        page.Objects.Add(Obj);
                    }
                    metaData.pages.Add(pageModel);

                }

                metaDataList.Add(metaData);
            }

            return metaDataList;
        }


        [HttpGet]
        
        public List<configuration> GetConfigurationFun()
        {
            dashboard_metadataEntities dashboard_metadataEntities = new dashboard_metadataEntities();
            return dashboard_metadataEntities.configurations.ToList();
        }

        [HttpPost]
        [AuthAttr]
        public IHttpActionResult SaveUserPushToken(UserPushTokenModel UserPushToken)
        {
          
            var headers = Request.Headers;
            if (headers.Contains("Token") && UserPushToken.UserPushToken !=null)
            {
                string tokenVale = headers.GetValues("Token").First();
                dashboard_metadataEntities dashboard_metadataEntities = new dashboard_metadataEntities();
                var User = dashboard_metadataEntities.Users.FirstOrDefault(token => token.UserToken == tokenVale);
                var pushToken = dashboard_metadataEntities.UserPNTokens.FirstOrDefault(push => push.PNToken == UserPushToken.UserPushToken);
                if(pushToken == null)
                    dashboard_metadataEntities.UserPNTokens.Add(new UserPNToken() { UserID = User.ID, PNToken = UserPushToken.UserPushToken });
                dashboard_metadataEntities.SaveChanges();
                return Ok();
            }
            else
            {
                UserFunction.LogError("Token : "+ headers.GetValues("Token").First()+"user push token : "+ UserPushToken.UserPushToken , new Exception());
                return Ok("Token : " + headers.GetValues("Token").First() + "   UserPushToken: " + UserPushToken.UserPushToken);
            }
        }

        [HttpPost]
        [AuthAttr]
        public IHttpActionResult DeletUserPushToken(UserPushTokenModel UserPushToken)
        {
            dashboard_metadataEntities dashboard_metadataEntities = new dashboard_metadataEntities();
            var entity = dashboard_metadataEntities.UserPNTokens.FirstOrDefault(mod => mod.PNToken == UserPushToken.UserPushToken);
            if (entity!=null)
            {
                dashboard_metadataEntities.UserPNTokens.Remove(entity);
                dashboard_metadataEntities.SaveChanges();
                return Ok();
            }
            else
            {
                UserFunction.LogError("user push token : " + UserPushToken.UserPushToken, new Exception());
                return Ok("Not found the UserPushToken");
            }
        }

        [HttpPost]
        public IHttpActionResult ReplaceUserPushToken(ReplaceUserPushTokenModel UserPushToken)
        {
            dashboard_metadataEntities dashboard_metadataEntities = new dashboard_metadataEntities();
            var entity = dashboard_metadataEntities.UserPNTokens.FirstOrDefault(mod => mod.PNToken == UserPushToken.OldUserPushToken);
            if (entity != null)
            {
                entity.PNToken = UserPushToken.NewUserPushToken;
                dashboard_metadataEntities.SaveChanges();
                return Ok();
            }
            else
            {
                UserFunction.LogError("user push token new  : " + UserPushToken.NewUserPushToken + "user push token Old  :" + UserPushToken.OldUserPushToken , new Exception());
                return Ok("Not found the UserPushToken");
            }
        }

        [AuthAttr]
        [HttpGet]
        public List<NameImg> GetImages()
        {
            List<NameImg> Returnlist = new List<NameImg>(); 
            dashboard_metadataEntities dashboard_metadataEntities = new dashboard_metadataEntities();
            var ListName = dashboard_metadataEntities.Names.ToList();
            foreach (var item in ListName)
            {
                NameImg nameImg = new NameImg();
                nameImg.name = item.Name1;
                var Images = dashboard_metadataEntities.NameImages.Where(img => img.NameID == item.ID).ToList();
                foreach (var item1 in Images)
                {
                    nameImg.images.Add(item1.NameOfImage);
                }
                Returnlist.Add(nameImg);
            }

            return Returnlist;
           
        }
    }
}
