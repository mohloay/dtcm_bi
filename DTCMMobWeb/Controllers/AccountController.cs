﻿using DTCMWebMobile.Generic_Function;
using DTCMWebMobile.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace DTCMWebMobile.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        [HttpGet]
        public ActionResult NewLogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult NewLogin(UserLogin model, string returnUrl)
        {

            if (ModelState.IsValid)
            {
                string USerGroups="";
                if (UserFunction.CheckUset(model ,ref USerGroups))
                {
                    string UserID = model.UserName;
                    string QlickTicket = UserFunction.GetTicket(model);
                    var GroupsSendNotifications = WebConfigurationManager.AppSettings["AdminGroupsSendNotifications"];
                    var GroupsUploadImages = WebConfigurationManager.AppSettings["AdminGroupsUploadImages"];
                    foreach (var item in USerGroups.Split('|'))
                    {
                        if (GroupsSendNotifications.Split(',').Contains(item))
                            Session["GroupsSendNotifications"] = true;
                        if (GroupsUploadImages.Split(',').Contains(item))
                            Session["GroupsUploadImages"] = true;
                    }  
                    Session["QlickTicket"] = QlickTicket;
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }

                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        [HttpGet]
        public ActionResult AdminLogin()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AdminLogin(UserLogin model, string returnUrl)
        {

            if (ModelState.IsValid)
            {
                string USerGroups = "";
                if (UserFunction.CheckUset(model, ref USerGroups))
                {
                    string UserID = model.UserName;
                    string QlickTicket = UserFunction.GetTicket(model);
                    var GroupsSendNotifications = WebConfigurationManager.AppSettings["AdminGroupsSendNotifications"];
                    var GroupsUploadImages = WebConfigurationManager.AppSettings["AdminGroupsUploadImages"];
                    foreach (var item in USerGroups.Split('|'))
                    {
                        Session["QlickTicket"] = QlickTicket;
                        if (GroupsSendNotifications.Split(',').Contains(item))
                        {
                            if (GroupsUploadImages.Split(',').Contains(item))
                            {
                                Session["GroupsUploadImages"] = true;
                            }
                            Session["GroupsSendNotifications"] = true;
                            return RedirectToAction("SendNotification", "Home");
                        }
                           
                        if (GroupsUploadImages.Split(',').Contains(item))
                        {
                            Session["GroupsUploadImages"] = true;
                            return RedirectToAction("UploadImages", "Home");
                        }
                            
                    }

                    ModelState.AddModelError("", "you are in authroized to access admin pages.");

                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
       


        [HttpPost]
        public ActionResult Login(UserLogin model, string returnUrl)
        {
            if (ModelState.IsValid)
            {

                string USerGroups = "";
                if (UserFunction.CheckUset(model, ref USerGroups))
                {
                    string UserID = model.UserName;
                   
                    string QlickTicket = UserFunction.GetTicket(model);
                    var SystemUsers = WebConfigurationManager.AppSettings["AdminUser"];
                    if (SystemUsers.Contains(model.UserName))
                        Session["SystemUser"] = true;
                    Session["QlickTicket"] = QlickTicket;
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
              
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        public ActionResult ServerLogin()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ServerLogin(UserLogin model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                string USerGroups = "";
                if (UserFunction.CheckUset(model, ref USerGroups))
                {
                    string QlickTicket = UserFunction.GetTicket(model);
                    string Server = WebConfigurationManager.AppSettings["QlikServerExtensions"];
                    return Redirect(Server+"?"+QlickTicket);
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

    }
}