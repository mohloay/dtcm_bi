using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace DTCMMobWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "DTCM",
                   url: "{*url}",
                   defaults: new { controller = "Home", action = "DTCM" } // The view that bootstraps Angular 2
                );
      routes.MapRoute(
            name: "NEWTPIDashBord",
                url: "{*url}",
                defaults: new { controller = "Home", action = "NEWTPIDashBord" } // The view that bootstraps Angular 2
             );
            routes.MapRoute(
           name: "VSDashbord",
               url: "{*url}",
               defaults: new { controller = "Home", action = "VSDashbord" } // The view that bootstraps Angular 2
            );
            routes.MapRoute(
           name: "NewPricingDashboard",
               url: "{*url}",
               defaults: new { controller = "Home", action = "NewPricingDashboard" } // The view that bootstraps Angular 2
            );
            routes.MapRoute(
           name: "NewOverSeas",
               url: "{*url}",
               defaults: new { controller = "Home", action = "NewOverSeas" } // The view that bootstraps Angular 2
            );
            routes.MapRoute(
           name: "NewHotelPipeLine",
               url: "{*url}",
               defaults: new { controller = "Home", action = "NewHotelPipeLine" } // The view that bootstraps Angular 2
            );
            routes.MapRoute(
           name: "NewBrandTracker",
               url: "{*url}",
               defaults: new { controller = "Home", action = "NewBrandTracker" } // The view that bootstraps Angular 2
            );

        }
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "GetTicket",
                routeTemplate: "api/{controller}/{action}/{username}/{password}",
                defaults: new { action = "Post", }
            );
            config.Routes.MapHttpRoute(
                        name: "API Default",
                        routeTemplate: "api/{controller}/{action}/{id}",
                        defaults: new { id = RouteParameter.Optional });


            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            config.Formatters.Remove(config.Formatters.XmlFormatter);

        }
    }
}
